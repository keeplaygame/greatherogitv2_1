﻿using UnityEngine;
using UnityEditor;

public class KeeplayTools
{
	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools/Clear Player Prefs.
	[MenuItem("Keeplay Tools/Game Cheats/Unlock Levels/2_Noah")]
	// Cria uma função static para o botão.
	private static void Noah()
	{
		string str_episodeName;
		str_episodeName = "NoahsArk";

		PlayerPrefs.SetInt("isEpisode" + str_episodeName + "LevelsUnLocked", 1);

		// Imprime mensagem no Console.
		Debug.Log ("Episode Noah comprado");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Unlock Levels/3_JosephOfEgypt")]
	// Cria uma função static para o botão.
	private static void JosephOfEgypt()
	{
		string str_episodeName;
		str_episodeName = "JosephOfEgypt";
		
		PlayerPrefs.SetInt("isEpisode" + str_episodeName + "LevelsUnLocked", 1);
		
		// Imprime mensagem no Console.
		Debug.Log ("Episode JosephOfEgypt comprado");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Unlock Levels/4_Moses")]
	// Cria uma função static para o botão.
	private static void Moses()
	{
		string str_episodeName;
		str_episodeName = "Moses";
		
		PlayerPrefs.SetInt("isEpisode" + str_episodeName + "LevelsUnLocked", 1);
		
		// Imprime mensagem no Console.
		Debug.Log ("Episode Moses comprado");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Unlock Levels/5_Joshua")]
	// Cria uma função static para o botão.
	private static void Joshua()
	{
		string str_episodeName;
		str_episodeName = "Joshua";
		
		PlayerPrefs.SetInt("isEpisode" + str_episodeName + "LevelsUnLocked", 1);

		// Imprime mensagem no Console.
		Debug.Log ("Episode Joshua comprado");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Unlock Levels/6_DavidAndGoliath")]
	// Cria uma função static para o botão.
	private static void DavidAndGoliath()
	{
		string str_episodeName;
		str_episodeName = "DavidAndGoliath";
		
		PlayerPrefs.SetInt("isEpisode" + str_episodeName + "LevelsUnLocked", 1);
		
		// Imprime mensagem no Console.
		Debug.Log ("Episode DavidAndGoliath comprado");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Unlock Levels/7_Jonah")]
	// Cria uma função static para o botão.
	private static void Jonah()
	{
		string str_episodeName;
		str_episodeName = "Jonah";
		
		PlayerPrefs.SetInt("isEpisode" + str_episodeName + "LevelsUnLocked", 1);

		// Imprime mensagem no Console.
		Debug.Log ("Episode Jonah comprado");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Unlock Levels/8_Daniel")]
	// Cria uma função static para o botão.
	private static void Daniel()
	{
		string str_episodeName;
		str_episodeName = "Daniel";
		
		PlayerPrefs.SetInt("isEpisode" + str_episodeName + "LevelsUnLocked", 1);
		
		// Imprime mensagem no Console.
		Debug.Log ("Episode Daniel comprado");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Unlock Levels/9_TheBirthOfJesus")]
	// Cria uma função static para o botão.
	private static void TheBirthOfJesus()
	{
		string str_episodeName;
		str_episodeName = "TheBirthOfJesus";
		
		PlayerPrefs.SetInt("isEpisode" + str_episodeName + "LevelsUnLocked", 1);
		
		// Imprime mensagem no Console.
		Debug.Log ("Episode TheBirthOfJesus comprado");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Unlock Levels/10_TheGreatHero")]
	// Cria uma função static para o botão.
	private static void TheGreatHero()
	{
		string str_episodeName;
		str_episodeName = "TheGreatHero";
		
		PlayerPrefs.SetInt("isEpisode" + str_episodeName + "LevelsUnLocked", 1);
		
		// Imprime mensagem no Console.
		Debug.Log ("Episode Great Hero comprado");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools/Clear Player Prefs.
	[MenuItem("Keeplay Tools/Language/English")]
	// Cria uma função static para o botão.
	private static void English()
	{
		// Grava no PlayerPrefs "LanguageSelected" que o idioma English foi selecionado.
		PlayerPrefs.SetInt("LanguageSelected", 2);
		
		// Seta o idioma para ingles
		Language.SwitchLanguage(LanguageCode.EN);
		
		// Imprime mensagem no Console.
		Debug.Log ("Language set to ENGLISH");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools/Clear Player Prefs.
	[MenuItem("Keeplay Tools/Language/Portuguese_BR")]
	// Cria uma função static para o botão.
	private static void Portuguese_BR()
	{
		// Grava no PlayerPrefs "LanguageSelected" que o idioma Portuguese foi selecionado.
		PlayerPrefs.SetInt("LanguageSelected", 1);
		
		// Seta o idioma para português.
		Language.SwitchLanguage(LanguageCode.PT);
		
		// Imprime mensagem no Console.
		Debug.Log ("Language set to PORTUGUESE_BR");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools/Clear Player Prefs.
	[MenuItem("Keeplay Tools/Clear PlayerPrefs")]
	// Cria uma função static para o botão.
	private static void DeleteAllPlayerPrefs()
	{
		// Deleta os PlayerPrefs.
		PlayerPrefs.DeleteAll();
		
		// Imprime mensagem no Console.
		Debug.Log ("PlayerPref DELETADOS");
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Complete All Levels/2_NoahsArks")]
	// Cria uma função static para o botão.
	private static void NoahsArks_All_Levels()
	{		
		string str_episodeName;
		str_episodeName = "NoahsArk";

		for(int i = 0; i < 9; i++)
		{
			// Grava que a fase foi completada
			PlayerPrefs.SetInt("isLevel" + str_episodeName + "_" + i + " Completed", 1);
		}
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Complete All Levels/3_JosephOfEgypt")]
	// Cria uma função static para o botão.
	private static void JosephOfEgypt_All_Levels()
	{		
		string str_episodeName;
		str_episodeName = "JosephOfEgypt";
		
		for(int i = 0; i < 9; i++)
		{
			// Grava que a fase foi completada
			PlayerPrefs.SetInt("isLevel" + str_episodeName + "_" + i + " Completed", 1);
		}
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Complete All Levels/4_Moses")]
	// Cria uma função static para o botão.
	private static void Moses_All_Levels()
	{		
		string str_episodeName;
		str_episodeName = "Moses";
		
		for(int i = 0; i < 9; i++)
		{
			// Grava que a fase foi completada
			PlayerPrefs.SetInt("isLevel" + str_episodeName + "_" + i + " Completed", 1);
		}
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Complete All Levels/6_DavidAndGoliath")]
	// Cria uma função static para o botão.
	private static void DavidAndGoliath_All_Levels()
	{		
		string str_episodeName;
		str_episodeName = "DavidAndGoliath";
		
		for(int i = 0; i < 9; i++)
		{
			// Grava que a fase foi completada
			PlayerPrefs.SetInt("isLevel" + str_episodeName + "_" + i + " Completed", 1);
		}
	}

	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools.
	[MenuItem("Keeplay Tools/Game Cheats/Complete All Levels/10_TheGreatHero")]
	// Cria uma função static para o botão.
	private static void TheGreatHero_All_Levels()
	{		
		string str_episodeName;
		str_episodeName = "TheGreatHero";
		
		for(int i = 0; i < 9; i++)
		{
			// Grava que a fase foi completada
			PlayerPrefs.SetInt("isLevel" + str_episodeName + "_" + i + " Completed", 1);
		}
	}
}