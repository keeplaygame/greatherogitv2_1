// Localization pacakge by Mike Hergaarden - M2H.nl
// DOCUMENTATION: http://www.m2h.nl/files/LocalizationPackage.pdf
// Thank you for buying this package!

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Alterado pelo Rguiotti para o projeto Great Hero da Mount Moriah.

public class LocalizedAsset : MonoBehaviour
{
    public Object localizeTarget;

    public void Awake()
    {
        LocalizeAsset(localizeTarget);
    }

    public void LocalizeAsset()
    {
        LocalizeAsset(localizeTarget);
    }

    //Only overrides current asset if a translation is available.
    public static void LocalizeAsset(Object target)
    {
        if (target == null)
        {
            Debug.LogError("LocalizedAsset target is null");
            return;
        }

        if (target.GetType() == typeof(GUITexture))
        {
            GUITexture gT = (GUITexture)target;
            if (gT.texture != null)
            {
                Texture text = (Texture)Language.GetAsset(gT.texture.name);
                if (text != null)
                    gT.texture = text;
            }
        }
		else if (target.GetType() == typeof(Image))
		{
			// CRIA imagem temporária do alvo.
			Image imgT = (Image)target;

			// VERIFICA se a imagem não é NULA.
			if (imgT.sprite != null)
			{
				// SETA a textura2d de acordo com o idioma selecionado.
				Texture2D texture2d = (Texture2D)Language.GetAsset(imgT.mainTexture.name);
					
				// VERIFICA se a texture2d existe, ou seja, não é nula.
				if (texture2d != null)
				{
					// Cria uma sprite e seta que a sprite é igual a texture2d.
					Sprite spr = Sprite.Create (texture2d, new Rect(0, 0, texture2d.width, texture2d.height), new Vector2(0.5f, 0.5f));
					
					// SETA que a sprite da imagem target é igual a sprite que foi criada a partir da texture2d no idioma selecionado.
					imgT.sprite = spr;

					// MANDA rodar a função de SetNativeSize do Componente Image.
					imgT.SetNativeSize();
				}
			}
		}
        else if (target.GetType() == typeof(Material))
        {
            Material mainT = (Material)target;
            if (mainT.mainTexture != null)
            {
                Texture text = (Texture)Language.GetAsset(mainT.mainTexture.name);
                if (text != null)
                    mainT.mainTexture = text;
            }
        }
        else if (target.GetType() == typeof(MeshRenderer))
        {
            MeshRenderer mainT = (MeshRenderer)target;
            if (mainT.material.mainTexture != null)
            {
                Texture text = (Texture)Language.GetAsset(mainT.material.mainTexture.name);
                if (text != null)
                    mainT.material.mainTexture = text;
            }
        }

        else
        {
            Debug.LogError("Could not localize this object type: " + target.GetType());
        }
    }
}