﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BlurEffect))]
[RequireComponent(typeof(GrayscaleEffect))]
public class ImageEffectsController : MonoBehaviour {

	// Define o script.
	private BlurEffect blurEffect;

	// Define o script.
	private GrayscaleEffect grayscaleEffect;

	void Start () {
		// Guarda o componente do BlurEffect.
		blurEffect = GetComponent<BlurEffect>();

		// Guarda o componente do BlurEffect.
		grayscaleEffect = GetComponent<GrayscaleEffect>();

		//
		SetImageEffectStatus("AllImageEffects", false);
	}

	/// <summary>
	/// Sets the image effect status.
	/// <para> strImageEffectName options: <para />
	/// <para> "BlurEffect" <para />
	/// <para> "GrayscaleEffect" <para />
	/// <para> "AllImageEffects" <para /> 
	/// </summary>
	/// <param name="strImageEffectName">String image effect name.</param>
	/// ddddd
	/// <param name="bIsStatusEnable">If set to <c>true</c> b is status enable.</param>
	public void SetImageEffectStatus (string strImageEffectName, bool bIsStatusEnable) {

		// Verifica qual imageEffect foi selecionado.
		switch (strImageEffectName) {

			case ("BlurEffect"):
				// Seta status do image effect.
				blurEffect.enabled = bIsStatusEnable;
					break;

			case ("GrayscaleEffect"):
				// Seta status do image effect.
				grayscaleEffect.enabled = bIsStatusEnable;			
				break;

			case ("AllImageEffects"):
				// Seta status do image effect.
				blurEffect.enabled = bIsStatusEnable;
				// Seta status do image effect.
				grayscaleEffect.enabled = bIsStatusEnable;			
				break;

		default:
			break;
		}
	}
}