﻿using UnityEngine;
using System.Collections;

public class MasterSetupController : MonoBehaviour {

	// Define o gameObject do DevOptions.
	[SerializeField] private GameObject go_devOptions;

	private DevOptions devOptions;

	// Define o int do ProjectEnviroment.
	[Header("ProjectEnviroment.0 = dev, 1 = production")]
	[SerializeField] private int i_projectEnvironment;

	// Define o int do i_isToPlayVictoryMusicAtFeedbackLevels1to9.
	[Header("0 = nao, 1 = sim")]
	[SerializeField] private int i_isToPlayVictoryMusicAtFeedbackLevels1to9;

	// Define int se é a 1a vez que o MasterSetupController foi inicializado.
	private int i_isFirstSetupMade;

	// Use this for initialization
	void Awake () {

		i_isFirstSetupMade = PlayerPrefs.GetInt("i_isFirstSetupMade");

		// Verifica se MasterSetupController já foi inicializado. 0 = não.
		if(i_isFirstSetupMade == 0) {
			SetImageEffectForFeedback ();
			
			// Seta o script de DevOptions.
			devOptions = go_devOptions.GetComponent<DevOptions>();
			
			// Seta qual é o Environment do projeto.
			devOptions.SetProjectEnvironment(i_projectEnvironment);
			
			// Seta se é i_isToPlayVictoryMusicAtFeedbackLevels1to9.
			devOptions.SetIfIsToPlayFeedbackVitoryMusicAtLevels1To9FromMasterSetupController (i_isToPlayVictoryMusicAtFeedbackLevels1to9);

			// Seta que o MasterSetupController já foi inicializado. 1 = sim. 
			PlayerPrefs.SetInt("i_isFirstSetupMade", 1);
		}
	}
	
	/// <summary>
	/// Sets the image effect for feedback.
	/// </summary>
	public void SetImageEffectForFeedback () {
		// Define PlayerPrefs do imageEffect da tela de feedback.
		// 0 = Grayscale Effect;
		// 1 = Blur Effect;
		// Altera o image Effect para o Blur Effect.
		PlayerPrefs.SetInt("ImageEffect", 1); 
	}
}