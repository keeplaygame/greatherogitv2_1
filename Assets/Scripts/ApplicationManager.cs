﻿using UnityEngine;
using System.Collections;

public class ApplicationManager : MonoBehaviour {

	void Update(){

		// VERIFICA se o botão de back do device android foi apertado.
		if (Input.GetKeyDown(KeyCode.Escape)) {
			#if UNITY_ANDROID || UNITY_STANDALONE
			Quit();
			#endif
		}			
	}

	// Função foi pega do package da asset store https://www.assetstore.unity3d.com/en/#!/content/25468
	public void Quit () {
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}
}