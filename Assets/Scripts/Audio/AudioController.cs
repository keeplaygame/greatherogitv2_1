﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour 
{
	[SerializeField]
	// Recebe e guarda GameObject que tem o script SFXCONTROLLER.
	private SFXController sfxController;

	[SerializeField]
	// Recebe e guarda o GameObject que tem o AudioOnButton.
	private GameObject go_audioOnButton;

	[SerializeField]
	// Recebe e guarda o GameObject que tem o AudioOffButton.
	private GameObject go_audioOffButton;

	void Awake ()
	{
		// Guarda o SFXCONTROLLER
		sfxController = GameObject.Find ("SFXController").GetComponent<SFXController>();
	}

	/// <summary>
	/// Sets the screen.
	/// </summary>
	public void SetAction (string strActionName)
	{
		// Verifica qual eh o Nome da Acao.
		switch (strActionName)
		{				
			// Caso seja AudioOn.
		case "AudioOn":
			
			// Toca o som de botao apertado.
			// audio.PlayOneShot(audio_button, 0.3f);
			SetPlaySFXAudio (0.3f);
			
			// Seta o volume igual a 1.
			AudioListener.volume = 1;

			break;
			
			// Caso seja AudioOff.
		case "AudioOff":
			
			// Seta o volume igual a 0
			AudioListener.volume = 0;

			break;

		default:

			break;
		}		
	}

	public void CheckAudioStatus ()
	{
		// Verifica se o Audio Status é ON
		if(AudioListener.volume == 1)
		{
			// Seta que o audio está ligado
			SetAudioButtonStatus("AudioOn");
		}
		
		// Verifica se o Audio Status é OFF
		else
		{
			// Seta que o audio está desligado
			SetAudioButtonStatus("AudioOff");
		}
	}

	/// <summary>
	/// Sets the audio button status.
	/// </summary>
	/// <param name="strAudioStatus">String audio status.</param>
	public void SetAudioButtonStatus(string strAudioStatus)
	{
		// Switch do status do Audio.
		switch(strAudioStatus)
		{
			
			// CASO seja o status ON.
		case("AudioOn"):
			
			// ATIVA o GameObject do Audio Button ON.
			go_audioOnButton.SetActive(true);
			
			// Seta o volume para 1.
			AudioListener.volume = 1;
			
			// DESATIVA o GameObject do Audio Button OFF.
			go_audioOffButton.SetActive(false);
			
			break;
			
			// CASO seja o status ON.
		case("AudioOff"):
			
			// DESATIVA o GameObject do Audio Button ON.
			go_audioOnButton.SetActive(false);
			
			// Seta o volume para 0.
			AudioListener.volume = 0;
			
			// ATIVA o GameObject do Audio Button OFF.
			go_audioOffButton.SetActive(true);
			
			break;
			
		default:
			
			break;
		}
	}

	/// <summary>
	/// Sets the play SFX audio.
	/// </summary>
	public void SetPlaySFXAudio (float fVolume)
	{
		// Executa função no SFXController para tocar áudio.
		sfxController.SetPlayAudioButton(fVolume);
	}

	/// <summary>
	/// Sets the play SFX audio.
	/// </summary>
	public void SetPlaySFXAudioDragWrongPosition (float fVolume)
	{
		// Executa função no SFXController para tocar áudio.
		sfxController.SetPlayAudioDragWrongPosition(fVolume);
	}

	/// <summary>
	/// Sets the play SFX audio.
	/// </summary>
	public void SetPlaySFXAudioDragRightPosition (float fVolume, int iDragIndex)
	{
		// Indica o index do efeito sonoro
		int iSFXIndex;

		// level controller
		LevelControllerExModuloController levelController;

		// Guarda o level controller
		levelController = GameObject.Find ("LevelController").GetComponent<LevelControllerExModuloController>();

		// Calcula o index do sfx
		iSFXIndex = iDragIndex + (4 * levelController.GetCurrentLevel ());

		// Executa função no SFXController para tocar áudio.
		sfxController.SetPlayAudioDragRightPosition(fVolume, iSFXIndex);
	}
}