﻿using UnityEngine;
using System.Collections;

// Para esse jogo, no caso da música não estamos usando AudioClip e alterando eles pois nós pausamos as músicas e depois despausamos elas.
// Caso contrário, utilizando a estrutura de AudioClip a música anterior é dado Stop, ou seja, quando ela volta a ser tocada, começa sempre do início.

public class MusicController : MonoBehaviour
{
	[SerializeField]
	private string[] array_str_music;

	[SerializeField]
	private GameObject[] array_go_music;

	/// <summary>
	/// Sets the music.
	/// </summary>
	/// <param name="strMusicName">String music name.</param>
	public void SetMusic (string strMusicName)
	{
		// Percorre todas as músicas
		for (int i = 0; i < array_str_music.Length; i++)
		{
			// Verifica se é a música que deve tocar
			if (array_str_music[i] == strMusicName)
			{
				// Verifica se a música não está tocando
				if (!array_go_music[i].audio.isPlaying)
				{
					// Toca a música
					array_go_music[i].audio.Play();
				}
			}

			// Verifica se está passando pela música de vitória
			else if (array_str_music[i] == "Victory")
			{
				// Para de tocar a música de vitória
				array_go_music[i].audio.Stop();
			}

			// Não é a música que deve tocar e nem a música de vitória
			else
			{
				// Pausa a música
				array_go_music[i].audio.Pause();
			}
		}
	}
}
