﻿using UnityEngine;
using System.Collections;

public class SFXController : MonoBehaviour 
{
	[SerializeField]
	// Recebe e guarda áudio que toca ao apertar algum botão.
	private AudioClip audio_button; 

	[SerializeField]
	// Recebe e guarda o áudio que toca quando ganha um novo selo
	private AudioClip audio_newStamp;

	[SerializeField]
	// Recebe e guarda o áudio que toca quando coloca um arrastável na posição errada
	private AudioClip audio_dragWrongPosition;

	[SerializeField]
	// Recebe e guarda o áudio que toca quando coloca o arrastável na posição certa
	private AudioClip[] array_audio_dragRightPlace = new AudioClip[40];

	/// <summary>
	/// Sets the play audio button.
	/// </summary>
	/// <param name="fVolume">F volume.</param>
	public void SetPlayAudioButton (float fVolume)
	{
		// Toca o som de botão apertado.
		audio.PlayOneShot(audio_button, fVolume);
	}

	public void SetPlayAudioNewStamp (float fVolume)
	{
		// Toca o som de novo selo
		audio.PlayOneShot(audio_newStamp, fVolume);
	}

	public void SetPlayAudioDragWrongPosition (float fVolume)
	{
		// Toca o som de arrastável na posição errada
		audio.PlayOneShot(audio_dragWrongPosition, fVolume);
	}

	public void SetPlayAudioDragRightPosition (float fVolume, int iSFXIndex)
	{
		// Toca o som de arrastável na posição errada
		audio.PlayOneShot(array_audio_dragRightPlace[iSFXIndex], fVolume);
	}
	
}