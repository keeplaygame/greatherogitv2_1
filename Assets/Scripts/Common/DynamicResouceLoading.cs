// DynamicResourceLoading package by Keeplay.

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// *********************
// Script criado pela Keeplay. Autores RGuiotti e FCosta. 2016. version 2.
// *********************

// ****************
//EXECUTA IN EDIT MODE
[ExecuteInEditMode]
public class DynamicResouceLoading : MonoBehaviour 
{
	[SerializeField]
	[Tooltip ("É o nome do próprio GameObject!!!")]
	// Nome da imagem.
	private string str_resourceName;

	[SerializeField]
	[Tooltip ("É o caminho onde está a imagem.")]
	// Nome do caminho da imagem
	private string str_imagePath;

	[SerializeField]
	[Tooltip ("NÃO Confundir!! É o GuiTexture, ou Image, ou Button Script.")]
	// Objeto a ser carregado.
	private Object obj_resourceType;

	void Awake ()
	{
		// PEGA o nome do GameObject. Dessa maneira fica automatizado.
		str_resourceName = this.gameObject.name;

		// Verifica se o tipo do resource é guitexture
		if(obj_resourceType.GetType() == typeof(GUITexture))
		{		
			// Guarda o guiTexture do objeto
			GUITexture guiTextureResource = (GUITexture)obj_resourceType;

			// Carrega a textura da resolução certa
			guiTextureResource.texture = Resources.Load(str_imagePath + str_resourceName) as Texture;
		}

		// Verifica se o tipo do resource é image
		else if(obj_resourceType.GetType() == typeof(Image))
		{
			// CRIA imagem temporária do alvo.
			Image imageResource = (Image)obj_resourceType;
			
			// SETA a textura2d de acordo com o idioma selecionado.
			Texture2D texture2d = new Texture2D(0, 0);

			// SETA a textura2d de acordo com a resoluçao selecionado.
			texture2d = Resources.Load(str_imagePath + str_resourceName) as Texture2D;	
							
			// Cria uma sprite e seta que a sprite é igual a texture2d.
			Sprite spr = Sprite.Create (texture2d, new Rect(0, 0, texture2d.width, texture2d.height), new Vector2(0.5f, 0.5f));
			
			// SETA que a sprite da imagem target é igual a sprite que foi criada a partir da texture2d no idioma selecionado.
			imageResource.sprite = spr;
			
			// SETA o nome da imagem
			imageResource.sprite.name = str_resourceName;
		}

		#region Button with SpriteSwap
		// Verifica se o tipo do resource é Button
		else if(obj_resourceType.GetType() == typeof(Button))
		{
			// Guarda o button do objeto
			Button button = (Button)obj_resourceType;

			// Textura2d normal de acordo com o idioma selecionado.
			Texture2D texture2dTarget = new Texture2D(0, 0);

			// Textura2d pressed de acordo com o idioma selecionado.
			Texture2D texture2dPressed = new Texture2D(0, 0);

			// Carrega a textura2d normal de acordo com a resoluçao selecionado.
			texture2dTarget = Resources.Load(str_imagePath + str_resourceName) as Texture2D;
				
			// Carrega a textura2d pressed de acordo com a resoluçao selecionado.
			texture2dPressed = Resources.Load(str_imagePath + str_resourceName + "_hover") as Texture2D;	


			#region Button with SpriteSwap
			// Verifica se a transição do botão é feita de troca de sprites
			if(button.transition == Selectable.Transition.SpriteSwap)
			{
				// Cria uma sprite e seta que a sprite é igual a texture2d.
				Sprite sprTarget = Sprite.Create (texture2dTarget, new Rect(0, 0, texture2dTarget.width, texture2dTarget.height), new Vector2(0.5f, 0.5f));

				// SETA que a sprite da imagem target é igual a sprite que foi criada a partir da texture2d no idioma selecionado.
				button.image.sprite = sprTarget;

				// SETA o nome do sprite do botão
				button.image.sprite.name = str_resourceName;

				// Cria uma sprite e seta que a sprite é igual a texture2d.
				Sprite sprPressed = Sprite.Create (texture2dPressed, new Rect(0, 0, texture2dPressed.width, texture2dPressed.height), new Vector2(0.5f, 0.5f));

				// Estado do sprite
				SpriteState spriteState = new SpriteState();

				// SETA que a sprite da imagem target é igual a sprite que foi criada a partir da texture2d no idioma selecionado.
				spriteState.pressedSprite = sprPressed;

				// Seta que o estado do sprite do botão é igual ao sprite temporário
				button.spriteState = spriteState;
				
				// SETA o nome do estado de pressionado para hover
				button.spriteState.pressedSprite.name = str_resourceName + "_hover";
			}
			#endregion

			// CASO seja transition do tipo COLOR TINT.
			else
			{
				// Cria uma sprite e seta que a sprite é igual a texture2d.
				Sprite sprTarget = Sprite.Create (texture2dTarget, new Rect(0, 0, texture2dTarget.width, texture2dTarget.height), new Vector2(0.5f, 0.5f));
				
				// SETA que a sprite da imagem target é igual a sprite que foi criada a partir da texture2d no idioma selecionado.
				button.image.sprite = sprTarget;
				
				// SETA o nome do sprite do botão
				button.image.sprite.name = str_resourceName;
			}
		}
		#endregion
//		// Verifica se o tipo do resource é ButtonControllerMainScene
//		else if(obj_resourceType.GetType() == typeof(ButtonControllerMainScene))
//		{
//			// Guarda ButtonControllerMainScene do objeto
//			ButtonControllerMainScene buttonControllerMainScene = (ButtonControllerMainScene)obj_resourceType;
//
//			// Verifica se a largura da resolução é 960
//			if(Screen.width == 9960)
//			{
//				// Carrega a textura normal do botão da resolução selecionada
//				buttonControllerMainScene.texture_normal = Resources.Load(str_imagePath + str_resourceName + "960x640") as Texture2D;
//
//				// Carrega a textura hover do botão da resolução selecionada
//				buttonControllerMainScene.texture_hover = Resources.Load(str_imagePath + str_resourceName + "960x640" + "_hover") as Texture2D;
//			}
//
//			// Verifica se a largura da resolução é 1024
//			else if(Screen.width == 1024 || Screen.width == 960)
//			{
//				// Carrega a textura normal do botão da resolução selecionada
//				buttonControllerMainScene.texture_normal = Resources.Load(str_imagePath + str_resourceName + "1024x768") as Texture2D;
//
//				// Carrega a textura hover do botão da resolução selecionada
//				buttonControllerMainScene.texture_hover = Resources.Load(str_imagePath + str_resourceName + "1024x768" + "_hover") as Texture2D;
//			}
//
//			// Verifica se a largura da resolução é 1136
//			else if(Screen.width == 1136)
//			{
//				// Carrega a textura normal do botão da resolução selecionada
//				buttonControllerMainScene.texture_normal = Resources.Load(str_imagePath + str_resourceName + "1136x640") as Texture2D;
//
//				// Carrega a textura hover do botão da resolução selecionada
//				buttonControllerMainScene.texture_hover = Resources.Load(str_imagePath + str_resourceName + "1136x640" + "_hover") as Texture2D;
//			}
//
//			// Verifica se a largura da resolução é 1334
//			else if(Screen.width == 1334)
//			{
//				// Carrega a textura normal do botão da resolução selecionada
//				buttonControllerMainScene.texture_normal = Resources.Load(str_imagePath + str_resourceName + "1334x750") as Texture2D;
//
//				// Carrega a textura hover do botão da resolução selecionada
//				buttonControllerMainScene.texture_hover = Resources.Load(str_imagePath + str_resourceName + "1334x750" + "_hover") as Texture2D;
//			}
//
//			// Verifica se a largura da resolução é 2048
//			else if(Screen.width == 2048)
//			{
//				// Verifica se é para carregar o resource de 2208x1242
//				if (b_isToLoadResource2208x1242IfIs2048x1536)
//				{
//					// Carrega a textura normal do botão da resolução selecionada
//					buttonControllerMainScene.texture_normal = Resources.Load(str_imagePath + str_resourceName + "2208x1242") as Texture2D;
//
//					// Carrega a textura hover do botão da resolução selecionada
//					buttonControllerMainScene.texture_hover = Resources.Load(str_imagePath + str_resourceName + "2208x1242" + "_hover") as Texture2D;
//				}
//				
//				// É para carregar o resource de 2048x1536 mesmo
//				else
//				{
//					// Carrega a textura normal do botão da resolução selecionada
//					buttonControllerMainScene.texture_normal = Resources.Load(str_imagePath + str_resourceName + "2048x1536") as Texture2D;
//
//					// Carrega a textura hover do botão da resolução selecionada
//					buttonControllerMainScene.texture_hover = Resources.Load(str_imagePath + str_resourceName + "2048x1536" + "_hover") as Texture2D;	
//				}
//			}
//
//			// Verifica se a largura da resolução é 2208
//			else if(Screen.width == 2208)
//			{
//				// Carrega a textura normal do botão da resolução selecionada
//				buttonControllerMainScene.texture_normal = Resources.Load(str_imagePath + str_resourceName + "2208x1242") as Texture2D;
//
//				// Carrega a textura hover do botão da resolução selecionada
//				buttonControllerMainScene.texture_hover = Resources.Load(str_imagePath + str_resourceName + "2208x1242" + "_hover") as Texture2D;
//			}
//
//			// Não é uma resolução suportada
//			else
//			{
//				// Carrega a textura normal do botão da menor resolução suportada
//				buttonControllerMainScene.texture_normal = Resources.Load(str_imagePath + str_resourceName + "960x640") as Texture2D;
//
//				// Carrega a textura hover do botão da menor resolução suportada
//				buttonControllerMainScene.texture_hover = Resources.Load(str_imagePath + str_resourceName + "960x640" + "_hover") as Texture2D;
//			}
//		}
//
//		// Verifica se o tipo do resource é AudioButton
//		else if(obj_resourceType.GetType() == typeof(AudioButton))
//		{
//			// Guarda AudioButton do objeto
//			AudioButton audioButton = (AudioButton)obj_resourceType;
//
//			// Verifica se a largura da resolução é 960
//			if(Screen.width == 9960)
//			{
//				// Carrega a textura normal do botão da resolução selecionada
//				audioButton.texture_normal = Resources.Load(str_imagePath + str_resourceName + "960x640") as Texture2D;
//
//				// Carrega a textura hover do botão da resolução selecionada
//				audioButton.texture_hover = Resources.Load(str_imagePath + str_resourceName + "960x640" + "_hover") as Texture2D;
//			}
//
//			// Verifica se a largura da resolução é 1024
//			else if(Screen.width == 1024 || Screen.width == 960)
//			{
//				// Carrega a textura normal do botão da resolução selecionada
//				audioButton.texture_normal = Resources.Load(str_imagePath + str_resourceName + "1024x768") as Texture2D;
//
//				// Carrega a textura hover do botão da resolução selecionada
//				audioButton.texture_hover = Resources.Load(str_imagePath + str_resourceName + "1024x768" + "_hover") as Texture2D;
//			}
//
//			// Verifica se a largura da resolução é 1136
//			else if(Screen.width == 1136)
//			{
//				// Carrega a textura normal do botão da resolução selecionada
//				audioButton.texture_normal = Resources.Load(str_imagePath + str_resourceName + "1136x640") as Texture2D;
//
//				// Carrega a textura hover do botão da resolução selecionada
//				audioButton.texture_hover = Resources.Load(str_imagePath + str_resourceName + "1136x640" + "_hover") as Texture2D;
//			}
//
//			// Verifica se a largura da resolução é 1334
//			else if(Screen.width == 1334)
//			{
//				// Carrega a textura normal do botão da resolução selecionada
//				audioButton.texture_normal = Resources.Load(str_imagePath + str_resourceName + "1334x750") as Texture2D;
//
//				// Carrega a textura hover do botão da resolução selecionada
//				audioButton.texture_hover = Resources.Load(str_imagePath + str_resourceName + "1334x750" + "_hover") as Texture2D;
//			}
//
//			// Verifica se a largura da resolução é 2048
//			else if(Screen.width == 2048)
//			{
//				// Verifica se é para carregar o resource de 2208x1242
//				if (b_isToLoadResource2208x1242IfIs2048x1536)
//				{
//					// Carrega a textura normal do botão da resolução selecionada
//					audioButton.texture_normal = Resources.Load(str_imagePath + str_resourceName + "2208x1242") as Texture2D;
//
//					// Carrega a textura hover do botão da resolução selecionada
//					audioButton.texture_hover = Resources.Load(str_imagePath + str_resourceName + "2208x1242" + "_hover") as Texture2D;
//				}
//				
//				// É para carregar o resource de 2048x1536 mesmo
//				else
//				{
//					// Carrega a textura normal do botão da resolução selecionada
//					audioButton.texture_normal = Resources.Load(str_imagePath + str_resourceName + "2048x1536") as Texture2D;
//
//					// Carrega a textura hover do botão da resolução selecionada
//					audioButton.texture_hover = Resources.Load(str_imagePath + str_resourceName + "2048x1536" + "_hover") as Texture2D;
//				}
//			}
//
//			// Verifica se a largura da resolução é 2208
//			else if(Screen.width == 2208)
//			{
//				// Carrega a textura normal do botão da resolução selecionada
//				audioButton.texture_normal = Resources.Load(str_imagePath + str_resourceName + "2208x1242") as Texture2D;
//
//				// Carrega a textura hover do botão da resolução selecionada
//				audioButton.texture_hover = Resources.Load(str_imagePath + str_resourceName + "2208x1242" + "_hover") as Texture2D;
//			}
//
//			// Não é uma resolução suportada
//			else
//			{
//				// Carrega a textura normal do botão da menor resolução suportada
//				audioButton.texture_normal = Resources.Load(str_imagePath + str_resourceName + "960x640") as Texture2D;
//
//				// Carrega a textura hover do botão da menor resolução suportada
//				audioButton.texture_hover = Resources.Load(str_imagePath + str_resourceName + "960x640" + "_hover") as Texture2D;
//			}
//		}
	}
}