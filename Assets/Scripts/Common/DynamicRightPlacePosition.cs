﻿using UnityEngine;
using System.Collections;

public class DynamicRightPlacePosition : MonoBehaviour 
{
	[SerializeField]
	[Header ("Posição correta na proporção 16:09")]
	// Posição correta do item na proporção 16:09
	private Vector2 vec2_rightPlacePosition16x09;

	[SerializeField]
	[Header ("Posição correta na proporção 04:03")]
	// Posição correta do item na proporção 04:03
	private Vector2 vec2_rightPlacePosition04x03;

	void Awake ()
	{
		// Verifica se é alguma resolução de proporção 16:09
		if (Screen.width == 1136 || Screen.width == 1334 || Screen.width == 2208)
		{
			// Seta a posição correta da imagem na proporção 16:09
			this.transform.position = new Vector3(vec2_rightPlacePosition16x09.x, vec2_rightPlacePosition16x09.y, this.transform.position.z);
		}

		// Verifica se é alguma resolução de proporção 4:3
		else if (Screen.width == 1024 || Screen.width == 2048)
		{
			// Seta a posição correta da imagem na proporção 04:03
			this.transform.position = new Vector3(vec2_rightPlacePosition04x03.x, vec2_rightPlacePosition04x03.y, this.transform.position.z);
		}
	}
}
