﻿using UnityEngine;
using System.Collections;

public class LateralBarResize : MonoBehaviour 
{
	[Header("Largura e Altura da imagem de referência")]
	[SerializeField]
	// Largura de referencia (maior imagem)
	private float f_referenceWidth;
	
	[SerializeField]
	// Altura de referencia (maior imagem)
	private float f_referenceHeight;

	// Largura da barra
	private float f_barWidth;

	void Awake () 
	{
		// Seta a largura da barra
		f_barWidth = 360;

		ResizeToResolution ();
	}

	/// <summary>
	/// Resizes to resolution.
	/// </summary>
	private void ResizeToResolution ()
	{
		// AspectRatio Referencial
		float fReferenceAspectRatio;
		
		// Verifica se a largura é maior ou igual que a altura
		if (f_referenceWidth >= f_referenceHeight)
		{
			// Calculo do aspectRatio referencial
			fReferenceAspectRatio = (float)f_referenceWidth/f_referenceHeight;
		}
		
		// A altura é maior
		else
		{
			// Calculo do aspectRatio
			fReferenceAspectRatio = (float)f_referenceHeight/f_referenceWidth;
		}
		
		// Proporção do tamanho da tela
		float fAspectRatio;
		
		// Verifica se a largura é maior ou igual que a altura
		if (Screen.width >= Screen.height)
		{
			// Calculo do aspectRatio
			fAspectRatio = (float)Screen.width/Screen.height;
		}
		
		// A altura é maior
		else
		{
			// Calculo do aspectRatio
			fAspectRatio = (float)Screen.height/Screen.width;
		}
		
		// Verifica se o aspect ratio é menos wide que a referência
		if (fAspectRatio < fReferenceAspectRatio)
		{
			this.gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (f_barWidth*0.85f, (f_referenceHeight+4));
		}
		
		// Verifica se o aspect ratio é mais wide que a referência
		else if (fAspectRatio > fReferenceAspectRatio)
		{
			this.gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (f_barWidth, (f_referenceHeight/(fAspectRatio/fReferenceAspectRatio))+4);
		} 
		
		// O aspect ratio é igual a referência
		else
		{
			this.gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (f_barWidth*0.95f, (f_referenceHeight+4));
		}

		// Chama a função para calcular o tamanho e posição dos arrastáveis
		GameObject.Find ("PanelDragItens").GetComponent<PanelDragController> ().CalculateDragsPositionAndSize (this.gameObject.GetComponent<RectTransform> ().sizeDelta);
	}
}
