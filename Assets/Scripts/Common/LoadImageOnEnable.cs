// LoadImageOnEnable package by Keeplay.

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// *********************
// Script criado pela Keeplay. Autor FCosta. 2017. version 1.
// *********************

// GameObject precisa ter o componente de imagem.
[RequireComponent (typeof (Image))]
public class LoadImageOnEnable : MonoBehaviour {

	[SerializeField]
	// Imagem a ser carregada.
	private Sprite sprite_backgroundLevelSelect;

	// Define o componente da Image.
	private Image image_component;

	void OnEnable () {
		// Seta qual componente é a image.
		image_component = this.GetComponent(typeof(Image)) as Image;

		// Seta a sprite.
		image_component.sprite = sprite_backgroundLevelSelect;

		// Seta para true o preserve Aspect do componente image.
		image_component.preserveAspect = true;
	}

	void OnDisable () {
		// Retira a imagem do componente.
		image_component.sprite = null;
	}
}