﻿using UnityEngine;
using System.Collections;

public class RightPlacePositionSetup : MonoBehaviour 
{
	// Posição correta do objeto
	private Vector2 vec2_rightPlacePosition;

	void Start()
	{
		// Objeto da sombra que possui a posição correta
		GameObject goShadowRightPlace = GameObject.Find(this.gameObject.name + "Shadow");

		// Pega a posição correta do objeto da sombra
		vec2_rightPlacePosition = new Vector2(goShadowRightPlace.transform.position.x, goShadowRightPlace.transform.position.y);

		// Seta a posição correta do objeto
		this.transform.position = new Vector3(vec2_rightPlacePosition.x, vec2_rightPlacePosition.y, this.transform.position.z);
	}
}