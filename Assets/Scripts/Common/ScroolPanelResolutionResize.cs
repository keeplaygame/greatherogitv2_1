﻿using UnityEngine;
using System.Collections;

public class ScroolPanelResolutionResize : MonoBehaviour 
{
	// Enumerador que indica qual a largura da resolução de referência
	private enum ReferenceResolution
	{
		Resolution1024x768,
		Resolution1334x750,
		Resolution2208x1242
	};

	[SerializeField]
	[Header ("Selecione a resolução que foi usada para montar a tela")]
	private ReferenceResolution referenceResolution;

	// Grid do scroll panel
	private GameObject go_grid;

	// Largura correta do item na resolução de referência
	private float f_panelWidthAtReferenceResolution;

	// Altura correta do item na resolução de referência
	private float f_panelHeightAtReferenceResolution;

	// Resolução de largura referência
	private float f_referenceWidthResolution;

	// Resolução de altura de referência
	private float f_referenceHeightResolution;

	// Largura do grid na resolução de referência
	private float f_gridWidthAtReferenceResolution;
	
	void Awake ()
	{
		// Guarda a largura utilizada na referência
		f_panelWidthAtReferenceResolution = this.gameObject.GetComponent<RectTransform>().sizeDelta.x;

		// Guarda a altura utilizada na referência
		f_panelHeightAtReferenceResolution = this.gameObject.GetComponent<RectTransform>().sizeDelta.y;

		// Guarda o grid do scrool panel
		go_grid = this .gameObject.transform.Find("Grid").gameObject;

		// Guarda a largura do grid utilizada na referência
		f_gridWidthAtReferenceResolution = go_grid.GetComponent<RectTransform>().sizeDelta.x;

		// Verifica qual é a resolução de referência
		switch (referenceResolution)
		{
			// Caso seja a resolução 1024x768
		case ReferenceResolution.Resolution1024x768:

			// Seta a largura da resolução de referência
			f_referenceWidthResolution = 1024;
			
			// Seta a altura da resolução de referência
			f_referenceHeightResolution = 768;

			break;

			// Caso seja a resolução 1334x750
		case ReferenceResolution.Resolution1334x750:

			// Seta a largura da resolução de referência
			f_referenceWidthResolution = 1334;
			
			// Seta a altura da resolução de referência
			f_referenceHeightResolution = 750;

			break;

			// Caso seja a resolução 2208x1242
		case ReferenceResolution.Resolution2208x1242:

			// Seta a largura da resolução de referência
			f_referenceWidthResolution = 2208;
			
			// Seta a altura da resolução de referência
			f_referenceHeightResolution = 1242;

			break;

		default:

			break;
		}

		// Largura do painel
		float fPanelWidth;

		// Altura do painel
		float fPanelHeigth;

		// Calcula e guarda a largura do painel
		fPanelWidth = (Screen.width/f_referenceWidthResolution) * f_panelWidthAtReferenceResolution;

		// Calcula e guarda a altura do painel
		fPanelHeigth = (Screen.height/f_referenceHeightResolution) * f_panelHeightAtReferenceResolution;

		// Seta a largura e comprimento do painel de acordo com a resolução
		this.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(fPanelWidth, fPanelHeigth);

		// Largura do grid
		float fGridWidth;

		// Calcula e guarda a largura do grid
		fGridWidth = (f_gridWidthAtReferenceResolution/f_referenceWidthResolution) * Screen.width;

		// Seta a nova largura do grid
		go_grid.GetComponent<RectTransform>().sizeDelta = new Vector2(fGridWidth, go_grid.GetComponent<RectTransform>().sizeDelta.y);

//		// Verifica se é alguma resolução de proporção 16:09
//		if (Screen.width == 1136 || Screen.width == 1334 || Screen.width == 2208)
//		{
//			// Seta a posição correta da imagem na proporção 16:09
//			this.transform.position = new Vector3(vec2_rightPlacePosition16x09.x, vec2_rightPlacePosition16x09.y, this.transform.position.z);
//		}
//		
//		// Verifica se é alguma resolução de proporção 4:3
//		else if (Screen.width == 1024 || Screen.width == 2048)
//		{
//			// Seta a posição correta da imagem na proporção 04:03
//			this.transform.position = new Vector3(vec2_rightPlacePosition04x03.x, vec2_rightPlacePosition04x03.y, this.transform.position.z);
//		}
	}
}
