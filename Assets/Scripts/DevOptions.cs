﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DevOptions : MonoBehaviour {

	// Define inteiro para qual imageEffect deve ser usado.
	// 0 = Grayscale Effect;
	// 1 = Blur Effect;
	private int i_imageEffect;

	// Define int para guardar o int de qual é o Environment do projeto.
	private int i_projectEnvironment;

	// Define int para guardar o int de i_isToPlayVictoryMusicAtFeedbackLevels1to9.
	// 0 = Nao;
	// 1 = Sim, tocar.
	private int i_isToPlayVictoryMusicAtFeedbackLevels1to9;

	// Define o GameObject do DevOptionsButton.
	[SerializeField] private GameObject go_devOptionsButton;

	// Define o componente de texto.
	[SerializeField] private Text text_imageEffectFeedbackButton;

	// Define o componente de texto.
	[SerializeField] private Text text_i_isToPlayVictoryMusicAtFeedbackLevels1to9OptionsButton;

	// Define int para contar cliques para ativar as opções de desenvolvedor.
	private int i_counterClicksToActivateDevOptions;

	void Start () {
		// Verifica qual é o enviroment escolhido.
		VerifyProjectEnvironmentChoose ();

		// Verifica qual imageEffect escolhido para a tela de feedback.
		VerifyImageEffectChoose ();

		// Verifica IfIsToPlayFeedbackVitoryMusicAtLevels1To9.
		VerifyIfIsToPlayFeedbackVitoryMusicAtLevels1To9 ();
	}

	/// <summary>
	/// Verifies the image effect choose.
	/// </summary>
	private void VerifyImageEffectChoose () {
		// Seta qual o int do ImageEffect.
		i_imageEffect = PlayerPrefs.GetInt("ImageEffect");

		// Verifica se o int é igual a 0 (Grayscale Effect)
		if(i_imageEffect == 0) {
			// Altera o texto do botão.
			text_imageEffectFeedbackButton.text = "Grayscale Effect";
		} else {
			// Altera o texto do botão.
			text_imageEffectFeedbackButton.text = "Blur Effect";
		}
	}

	/// <summary>
	/// Sets the image effect for feedback.
	/// </summary>
	public void SetImageEffectForFeedback () {
		// Verifica se o int é igual a 0 (Grayscale Effect)
		if(i_imageEffect == 0) {
			// Altera o image Effect para o Blur Effect.
			PlayerPrefs.SetInt("ImageEffect", 1); 

			// Altera o texto do botão.
			text_imageEffectFeedbackButton.text = "Blur Effect";

			// Seta qual o int do ImageEffect.
			i_imageEffect = 1;
		} else {
			// Altera o image Effect para o Grayscale Effect.
			PlayerPrefs.SetInt("ImageEffect", 0); 

			// Altera o texto do botão.
			text_imageEffectFeedbackButton.text = "Grayscale Effect";

			// Seta qual o int do ImageEffect.
			i_imageEffect = 0;
		}
	}

	/// <summary>
	/// Sets the project environment.
	/// <para> 0 = development environment</para>
	/// <para> 1 = production environment</para>
	/// </summary>
	/// <param name="iProjectEnvironment">I project environment.</param>
	public void SetProjectEnvironment (int iProjectEnvironment) {

		// Altera o Environment do projeto.
		PlayerPrefs.SetInt("ProjectEnvironment", iProjectEnvironment);	
	}

	/// <summary>
	/// Verifies the project environment choose.
	/// </summary>
	private void VerifyProjectEnvironmentChoose () {
		// Seta qual o int do i_projectEnvironment.
		i_projectEnvironment = PlayerPrefs.GetInt("ProjectEnvironment");

		// Verifica se é o project environment de development.
		if(i_projectEnvironment == 0) {
			// Ativa o botão.
			go_devOptionsButton.SetActive(true);
		} else {
			// Desativa o botão.
			go_devOptionsButton.SetActive(false);
		}
	}

	/// <summary>
	/// Activates the button development environment.
	/// </summary>
	public void ActivateButtonDevelopmentEnvironment () {

		i_counterClicksToActivateDevOptions += 1;

		Debug.Log(i_counterClicksToActivateDevOptions);

		// Verifica se o número de cliques é maior que 2.
		if(i_counterClicksToActivateDevOptions > 2) {
			// Ativa o development environment.
			SetProjectEnvironment(0);
			
			// Verifica qual é o enviroment escolhido.
			VerifyProjectEnvironmentChoose ();
		} 

		if (i_counterClicksToActivateDevOptions > 4) {
			// Ativa o development environment.
			SetProjectEnvironment(1);
			
			// Verifica qual é o enviroment escolhido.
			VerifyProjectEnvironmentChoose ();

			// Zera o contador de cliques do botão.
			i_counterClicksToActivateDevOptions = 0;
		}
	}

	/// <summary>
	/// Sets the project environment.
	/// <para> 0 = development environment</para>
	/// <para> 1 = production environment</para>
	/// </summary>
	/// <param name="iProjectEnvironment">I project environment.</param>
	public void SetIfIsToPlayFeedbackVitoryMusicAtLevels1To9FromMasterSetupController (int iIsToPlayVictoryMusicAtFeedbackLevels1to9) {
		
		// Altera o i_isToPlayVictoryMusicAtFeedbackLevels1to9.
		PlayerPrefs.SetInt("i_isToPlayVictoryMusicAtFeedbackLevels1to9", iIsToPlayVictoryMusicAtFeedbackLevels1to9);	
	}

	/// <summary>
	/// Verifies if is to play feedback vitory music at levels1 to9.
	/// </summary>
	private void VerifyIfIsToPlayFeedbackVitoryMusicAtLevels1To9 () {
		// Seta qual o int do i_isToPlayVictoryMusicAtFeedbackLevels1to9.
		i_isToPlayVictoryMusicAtFeedbackLevels1to9 = PlayerPrefs.GetInt("i_isToPlayVictoryMusicAtFeedbackLevels1to9");
		
		// Verifica se o int é igual a 0 (Não é para tocar a música)
		if(i_isToPlayVictoryMusicAtFeedbackLevels1to9 == 0) {
			// Altera o texto do botão.
			text_i_isToPlayVictoryMusicAtFeedbackLevels1to9OptionsButton.text = "Não tocar";
		} else {
			// Altera o texto do botão.
			text_i_isToPlayVictoryMusicAtFeedbackLevels1to9OptionsButton.text = "Tocar em todos os feedbacks";
		}
	}

	/// <summary>
	/// Sets if is to play feedback vitory music at levels1 to9.
	/// </summary>
	public void SetIfIsToPlayFeedbackVitoryMusicAtLevels1To9  () {
		// Verifica se o int é igual a 0 (Não tocar)
		if(i_isToPlayVictoryMusicAtFeedbackLevels1to9 == 0) {
			// Altera o i_isToPlayVictoryMusicAtFeedbackLevels1to9 para true, ou seja, tocar em todos os feedbacks.
			PlayerPrefs.SetInt("i_isToPlayVictoryMusicAtFeedbackLevels1to9", 1); 
			
			// Altera o texto do botão.
			text_i_isToPlayVictoryMusicAtFeedbackLevels1to9OptionsButton.text = "Tocar em todos os feedbacks";
			
			// Seta qual o int do i_isToPlayVictoryMusicAtFeedbackLevels1to9.
			i_isToPlayVictoryMusicAtFeedbackLevels1to9 = 1;
		} else {
			// Altera o i_isToPlayVictoryMusicAtFeedbackLevels1to9 para false, ou seja, Não Tocar.
			PlayerPrefs.SetInt("i_isToPlayVictoryMusicAtFeedbackLevels1to9", 0); 
			
			// Altera o texto do botão.
			text_i_isToPlayVictoryMusicAtFeedbackLevels1to9OptionsButton.text = "Não tocar";
			
			// Seta qual o int do i_isToPlayVictoryMusicAtFeedbackLevels1to9.
			i_isToPlayVictoryMusicAtFeedbackLevels1to9 = 0;
		}
	}
}