using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class DragMe : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	[Header("Imagem que vai em cima da sombra")]
	// Imagem que aparece em cima da sombra
	public Sprite spr_imageOnShadow;

	private bool dragOnSurfaces = true;
	
	private GameObject m_DraggingIcon;
	private RectTransform m_DraggingPlane;

	// Guarda o audio controller
	private AudioController audioController;

	void Awake () 
	{
		// Guarda o audio controller
		audioController = GameObject.Find ("AudioController").GetComponent<AudioController>();
	}

	/// <summary>
	/// Updates the size and position.
	/// </summary>
	/// <param name="iDragPosition">I drag position.</param>
	/// <param name="fReferenceHeight">F reference height.</param>
	/// <param name="fTotalHeigth">F total heigth.</param>
	public void UpdateSizeAndPosition(int iDragPosition, float[] array_fDragHeigthPercentagem, float fScalingFactor, float fTopAndBotSpaces, float fDragSpaces, float fAvailableWidth)
	{
		// Guarda a escala atual do arrastável
		Vector3 vec3Scale = this.gameObject.GetComponent<RectTransform> ().localScale;

		// Aplica o fator de escala
		this.gameObject.GetComponent<RectTransform> ().localScale = new Vector3 (vec3Scale.x * fScalingFactor, vec3Scale.z * fScalingFactor, vec3Scale.z * fScalingFactor);

		// Verifica se a largura da imagem é maior que a largura da barra lateral, considerando os espaços
		if (this.gameObject.GetComponent<RectTransform>().sizeDelta.x * this.gameObject.GetComponent<RectTransform> ().localScale.x  > fAvailableWidth)
		{
			// Calcula o fator de escala de x
			float fXScalingFactor = fAvailableWidth/(this.gameObject.GetComponent<RectTransform>().sizeDelta.x * this.gameObject.GetComponent<RectTransform> ().localScale.x);
			
			// Aplica o fator de escala
			this.gameObject.GetComponent<RectTransform>().localScale = new Vector3(this.gameObject.GetComponent<RectTransform>().localScale.x*fXScalingFactor, this.gameObject.GetComponent<RectTransform>().localScale.y*fXScalingFactor, this.gameObject.GetComponent<RectTransform>().localScale.z*fXScalingFactor);
		}

		// Guarda a ancorâ em Y
		float fAnchorY;

		// Verifica qual é o arrastável
		switch (iDragPosition) 
		{
		case 0:

			// Calcula a âncora em y do arrastável
			fAnchorY = 1 - (fTopAndBotSpaces + array_fDragHeigthPercentagem[0]/2);

			this.gameObject.GetComponent<RectTransform> ().anchorMin = new Vector2(0.5f, fAnchorY);
			this.gameObject.GetComponent<RectTransform> ().anchorMax = new Vector2(0.5f, fAnchorY);

			break;

		case 1:

			// Calcula a âncora em y do arrastável
			fAnchorY = 1-(fTopAndBotSpaces + array_fDragHeigthPercentagem[0] + fDragSpaces + array_fDragHeigthPercentagem[1]/2);

			this.gameObject.GetComponent<RectTransform> ().anchorMin = new Vector2(0.5f, fAnchorY);
			this.gameObject.GetComponent<RectTransform> ().anchorMax = new Vector2(0.5f, fAnchorY);

			break;

		case 2:

			// Calcula a âncora em y do arrastável
			fAnchorY = 1-(fTopAndBotSpaces + array_fDragHeigthPercentagem[0] + fDragSpaces + array_fDragHeigthPercentagem[1] + fDragSpaces + array_fDragHeigthPercentagem[2]/2);
			
			this.gameObject.GetComponent<RectTransform> ().anchorMin = new Vector2(0.5f, fAnchorY);
			this.gameObject.GetComponent<RectTransform> ().anchorMax = new Vector2(0.5f, fAnchorY);

			break;

		case 3:

			// Calcula a âncora em y do arrastável
			fAnchorY = fTopAndBotSpaces + array_fDragHeigthPercentagem[3]/2;
			
			this.gameObject.GetComponent<RectTransform> ().anchorMin = new Vector2(0.5f, fAnchorY);
			this.gameObject.GetComponent<RectTransform> ().anchorMax = new Vector2(0.5f, fAnchorY);

			break;

		default:

			break;
		}
	}
	
	public void OnBeginDrag(PointerEventData eventData)
	{
		var canvas = FindInParents<Canvas>(gameObject);
		if (canvas == null)
			return;
		
		// We have clicked something that can be dragged.
		// What we want to do is create an icon for this.
		m_DraggingIcon = new GameObject("icon");

		m_DraggingIcon.transform.SetParent (canvas.transform, false);
		m_DraggingIcon.transform.SetAsLastSibling();

		// Aumenta 15% a imagem da barra
		m_DraggingIcon.transform.localScale = this.gameObject.GetComponent<RectTransform> ().localScale*1.25f;
		
		var image = m_DraggingIcon.AddComponent<Image>();
		// The icon will be under the cursor.
		// We want it to be ignored by the event system.
		CanvasGroup group = m_DraggingIcon.AddComponent<CanvasGroup>();
		group.blocksRaycasts = false;
		
		image.sprite = GetComponent<Image>().sprite;
		image.SetNativeSize();
		
		if (dragOnSurfaces)
			m_DraggingPlane = transform as RectTransform;
		else
			m_DraggingPlane = canvas.transform as RectTransform;
		
		SetDraggedPosition(eventData);

		// Coloca a cor do objeto arrastavel transparente
		GetComponent<Image> ().color = new Color(1, 1, 1, 0);
	}
	
	public void OnDrag(PointerEventData data)
	{
		if (m_DraggingIcon != null)
			SetDraggedPosition(data);
	}
	
	private void SetDraggedPosition(PointerEventData data)
	{
		if (dragOnSurfaces && data.pointerEnter != null && data.pointerEnter.transform as RectTransform != null)
			m_DraggingPlane = data.pointerEnter.transform as RectTransform;
		
		var rt = m_DraggingIcon.GetComponent<RectTransform>();
		Vector3 globalMousePos;
		if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlane, data.position, data.pressEventCamera, out globalMousePos))
		{
			rt.position = globalMousePos;
			rt.rotation = m_DraggingPlane.rotation;
		}
	}
	
	public void OnEndDrag(PointerEventData eventData)
	{
		// Coloca a cor do objeto arrastavel para opaco
		GetComponent<Image> ().color = new Color(1, 1, 1, 1);

		if (m_DraggingIcon != null)
			Destroy(m_DraggingIcon);

		// Toca o som de erro
		audioController.SetPlaySFXAudioDragWrongPosition(0.7f);

	}

	/// <summary>
	/// Determines whether this instance is to set active the specified bIsToSetActive.
	/// </summary>
	/// <returns><c>true</c> if this instance is to set active the specified bIsToSetActive; otherwise, <c>false</c>.</returns>
	/// <param name="bIsToSetActive">If set to <c>true</c> b is to set active.</param>
	public void IsToSetActive (bool bIsToSetActive)
	{
		// Seta se é para ativar ou desativar o objeto
		this.gameObject.SetActive(bIsToSetActive);
	}
	
	static public T FindInParents<T>(GameObject go) where T : Component
	{
		if (go == null) return null;
		var comp = go.GetComponent<T>();
		
		if (comp != null)
			return comp;
		
		Transform t = go.transform.parent;
		while (t != null && comp == null)
		{
			comp = t.gameObject.GetComponent<T>();
			t = t.parent;
		}
		return comp;
	}
}
