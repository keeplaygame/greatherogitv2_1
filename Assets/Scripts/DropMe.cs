using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Reflection;
using System.Collections;

public class DropMe : MonoBehaviour, IDropHandler
{
	public Image receivingImage;

	// Define o gameObject quando o jogador acerta o encaixe da peça.
	private GameObject go_correctParticle;

	// Guarda o audio controller
	private AudioController audioController;

	private float f_incrementalAlphaTime;

	void Awake () 
	{
		// Guarda gameObject que tem a particula correta.
		go_correctParticle = GameObject.FindGameObjectWithTag ("CorrectParticle");

		// Guarda o audio controller
		audioController = GameObject.Find ("AudioController").GetComponent<AudioController>();

		// Seta tempo de incremento do alpha.
		f_incrementalAlphaTime = 0.05f;
	}

	public void OnDrop(PointerEventData data)
	{
		if (receivingImage == null)
			return;
		
		Sprite dropSprite = GetDropSprite (data);
		if (dropSprite != null)
		{
			// Seta o alpha para 0, ou seja, transparencia.
			receivingImage.color = new Color (receivingImage.color.r, receivingImage.color.g, receivingImage.color.b, 0f);

			receivingImage.overrideSprite = dropSprite;

			// Inicia corrotina.
			StartCoroutine("dropImageFadeIn");

			// Destroi o ícone arrastável
			Destroy(GameObject.Find("icon"));
		}
	}

	/// <summary>
	/// Drops the image fade in.
	/// </summary>
	/// <returns>The image fade in.</returns>
	private IEnumerator dropImageFadeIn () {

		yield return new WaitForSeconds (f_incrementalAlphaTime);

		receivingImage.color = new Color (receivingImage.color.r, receivingImage.color.g, receivingImage.color.b, receivingImage.color.a + 0.05f);

		if (receivingImage.color.a != 1){
			StartCoroutine("dropImageFadeIn");
		}

	}
	
	private Sprite GetDropSprite(PointerEventData data)
	{
		var originalObj = data.pointerDrag;
		if (originalObj == null)
			return null;

		// Verifica se o nome do arrastavel é diferente do nome da sombra
		if (originalObj.name.Replace("bl_", "") != this.gameObject.name.Replace("Shadow", ""))
		{
			return null;
		}

		// O nome do arrastável e da sombra são iguais
		else
		{
			// Pega a imagem do arrastável
			var srcImage = originalObj.GetComponent<DragMe>().spr_imageOnShadow;
			if (srcImage == null)
				return null;

			// Desativa o item arrastável
			originalObj.SetActive(false);

			// Toca o som de acerto
			audioController.SetPlaySFXAudioDragRightPosition(0.4f, originalObj.gameObject.transform.GetSiblingIndex());

			// Guarda a posição da própria peça.
			Vector2 vec2_newPosition = new Vector2(this.transform.position.x, this.transform.position.y);

			// Altera a posição da partícula.
			go_correctParticle.transform.position = vec2_newPosition;

			// Ativa a partícula para ser executada.
			go_correctParticle.particleSystem.Play();

			// Seta que o item foi colocado na posiçao correta
			GameObject.Find("LevelController").GetComponent<LevelControllerExModuloController>().SetRightItem();

			// Retorna o sprite do arrastável
			return srcImage;
		}
	}
}
