﻿using UnityEngine;
using System.Collections;

public class EpisodeController : MonoBehaviour 
{
	// Enumerador que indica qual e o episódio
	private enum CurrentEpisode
	{
		AdamAndEve,
		NoahsArk,
		JosephOfEgypt,
		Moses,
		Joshua,
		DavidAndGoliath,
		Jonah,
		Daniel,
		TheBirthOfJesus,
		TheGreatHero
	}

	[Header("Episódio Atual")]
	[SerializeField]
	// Episódio atual
	private CurrentEpisode enum_currentEpisode;

	// Define o script do ScreenController_episode2.
	private ScreenController_episode2 screenController_episode;

	// Recebe o nome do episódio
	private string str_episodeName;

	[Header("Botões das fases SEM estrelas")]
	[SerializeField]
	// Recebe os botoes das fases sem estrela
	private GameObject[] array_go_levelButtonWithoutStars = new GameObject[10];

	[Header("Botões das fases COM estrelas")]
	[SerializeField]
	// Recebe os botoes das fases com estrela
	private GameObject[] array_go_levelButtonWithStars = new GameObject[10];

	[Header("Botões CINZAS das fases")]
	[SerializeField]
	// Recebe os botoes das fases cinzas
	private GameObject[] array_go_levelButtonGray = new GameObject[10];

	[Header("Cadeados das fases travadas")]
	[SerializeField]
	// Recebe os cadeados das fases travadas
	private GameObject[] array_go_levelPadlock = new GameObject[5];
	
	[Header("Botão de Compra")]
	[SerializeField]
	// Recebe o botão de compra do in app
	private GameObject go_buyButton;

	// Indica se as fases do episódio estão destravadas
	private bool b_isEpisodeLevelsUnLocked;

	// Indica se as fases foram completadas
	private bool[] array_b_isLevelsCompleted = new bool[10];

	void Start ()
	{
		// Guarda o nome do episódio
		str_episodeName = enum_currentEpisode.ToString ();

		// Guarda o script que está no GameObject ScreenControllerEpisode.
		screenController_episode = GameObject.Find("ScreenControllerEpisode").GetComponent<ScreenController_episode2>();

		// Percorre todas as fases
		for (int i = 0; i < array_b_isLevelsCompleted.Length; i++)
		{
			// Verifica se nao completou a fase do episódio
			if (PlayerPrefs.GetInt("isLevel" + str_episodeName + "_" + i + " Completed") == 0)
			{
				// Seta que nao completou a fase
				array_b_isLevelsCompleted[i] = false;
			}
			
			// Completou a fase
			else
			{
				// Seta que completou a fase
				array_b_isLevelsCompleted[i] = true;
			} 
		}

		// VERIFICA se o nome do episodio é AdamAndEve. 
		if(str_episodeName == "AdamAndEve")
		{
			// SETA que o playerPref que lida com as travas das fases do episódio AdamAndEve é igual a 1. Isso porque o episódio do Adão e Eva é totalmente
			// aberto para os usuários. É o único episódio que é totalmente gratuito.
			PlayerPrefs.SetInt("isEpisode" + str_episodeName + "LevelsUnLocked", 1);
		}

		// Verifica se as fases do episódio NÃO estão destravadas
		if (PlayerPrefs.GetInt("isEpisode" + str_episodeName + "LevelsUnLocked") == 0)
		{
			// Seta que as fases do episódio não estão destravadas
			b_isEpisodeLevelsUnLocked = false;

			// Ativa o botão de compra
			go_buyButton.SetActive(true);

			// Percorre todos os cadeados
			for (int i = 0; i < array_go_levelPadlock.Length; i++)
			{
				// Ativa o cadeado
				array_go_levelPadlock[i].SetActive(true);
			}
		}
		
		// As fases do episódio estão destravadas
		else
		{
			// Seta que as fases do episódio estão destravadas
			b_isEpisodeLevelsUnLocked = true;
		}

		// Atualiza as fases
		UpdateLevels();
	}

	/// <summary>
	/// Gets the name of the string episode.
	/// </summary>
	/// <returns>The string episode name.</returns>
	public string GetStrEpisodeName ()
	{
		// Retorna o nome do episódio
		return str_episodeName;
	}

	/// <summary>
	/// Gets the current episode.
	/// </summary>
	/// <returns>The current episode.</returns>
	public int GetCurrentEpisode ()
	{
		// retorna o episódio atual
		return (int)enum_currentEpisode;
	}

	/// <summary>
	/// Updates the levels.
	/// </summary>
	public void UpdateLevels ()
	{
		// Verifica se as fases estão destravadas
		if (b_isEpisodeLevelsUnLocked) 
		{
			// Percorre todos os cadeados
			for (int i = 0; i < array_go_levelPadlock.Length; i++)
			{
				// Desativa o cadeado
				array_go_levelPadlock[i].SetActive(false);
			}
		}

		// Percorre todas as fases
		for (int i = 0; i < array_b_isLevelsCompleted.Length; i++)
		{
			// Desativa os botões das fases
			array_go_levelButtonWithoutStars[i].SetActive(false);
			array_go_levelButtonWithStars[i].SetActive(false);
			array_go_levelButtonGray[i].SetActive(false);

			// Verifica se a fase foi completada
			if (array_b_isLevelsCompleted[i])
		    {
				// Ativa o botão com estrelas
				array_go_levelButtonWithStars[i].SetActive(true);
			}

			// A fase não foi completada
			else
			{
				// Verifica se as fases do episódio NÃO estão destravadas E se está na sexta fase
				if (!b_isEpisodeLevelsUnLocked && i == 5)
				{
					// Ativa o botão cinza
					array_go_levelButtonGray[i].SetActive(true);
				}

				else
				{
					// Ativa o botão sem estrelas
					array_go_levelButtonWithoutStars[i].SetActive(true);
				}

				// Percorre as fases restantes
				for (int z = i+1; z < array_b_isLevelsCompleted.Length; z++)
				{
					// Ativa o botão cinza
					array_go_levelButtonGray[z].SetActive(true);
				}

				// Termina o for
				return;
			}
		}
	}

	/// <summary>
	/// Restarts the levels.
	/// </summary>
	public void RestartLevels ()
	{
		// Percorre todas as fases
		for (int i = 0; i < array_b_isLevelsCompleted.Length; i++)
		{
			// Seta que nao completou a fase do episódio
			PlayerPrefs.SetInt("isLevel" + str_episodeName + "_" + i + " Completed", 0);
			
			// Seta que nao completou a fase
			array_b_isLevelsCompleted[i] = false;
		}

		// Atualiza as fases
		UpdateLevels ();
	}

	/// <summary>
	/// Purchaseds the levels.
	/// </summary>
	public void PurchasedLevels ()
	{
		// Seta que as fases do episodio estao destravadas
		b_isEpisodeLevelsUnLocked = true;

		// Desativa o botão de compra
		go_buyButton.SetActive(false);

		// Percorre todos os cadeados
		for (int i = 0; i < array_go_levelPadlock.Length; i++)
		{
			// Desativa o cadeado
			array_go_levelPadlock[i].SetActive(false);
		}

		// Atualiza as fases
		UpdateLevels();

		// Verifica se o level 5 já foi completado.
		if(array_b_isLevelsCompleted[4]) {
			// Seta que é para ir para o level 6.
			screenController_episode.SetLevelScreen(5);
		}
	}
	
	/// <summary>
	/// Sets the level completed.
	/// </summary>
	public void SetLevelCompleted (int iLevel)
	{
		// Grava que a fase foi completada
		PlayerPrefs.SetInt("isLevel" + str_episodeName + "_" + iLevel + " Completed", 1);

		// Seta que a fase foi completada
		array_b_isLevelsCompleted[iLevel] = true;
	}
}