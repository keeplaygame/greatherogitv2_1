﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//
// LISTA DE EPISÓDIOS
// 1 - Davi
//

public class EpisodesSelection : MonoBehaviour 
{	
	[SerializeField]
	// Recebe e guarda o sfx controller
	private SFXController sfxController;

	[SerializeField]
	private GameObject go_loadingBackgroundImage;

	// Contador da animaçao do scroll rect
	//private int i_scrollRectAnimationCounter;

	void Start ()
	{
		// Seta que o contador inicia em 0
		//i_scrollRectAnimationCounter = 0;
	}

	/// <summary>
	/// Sets the episode to go.
	/// </summary>
	/// <param name="strEpisodeToGo">String episode to go.</param>
	public void SetEpisodeToGo (string strEpisodeToGo)
	{
//		// Inicia a corotina para carregar a proxima cena
//		StartCoroutine("ShowActivity", strEpisodeToGo);

		SceneController.LoadLevelEpisode (strEpisodeToGo);
	}

	/// <summary>
	/// Starts at last episode selected.
	/// </summary>
	public void StartAtLastEpisodeSelected ()
	{
		// Verifica se existe um episódio selecionado anteriormente
		if (PlayerPrefs.GetInt ("iLastEpisodeSelected", -1) >= 0) 
		{
			switch (PlayerPrefs.GetInt ("iLastEpisodeSelected", -1))
			{
			case 0:

				// Seta a posição do scroll rect no episódio selecionado anteriormente
				GameObject.Find ("ScrollPanel").GetComponent<ScrollRect>().horizontalNormalizedPosition = 0;

				break;

			case 1:

				// Seta a posição do scroll rect no episódio selecionado anteriormente
				GameObject.Find ("ScrollPanel").GetComponent<ScrollRect>().horizontalNormalizedPosition = 0;

				break;

			case 2:

				// Seta a posição do scroll rect no episódio selecionado anteriormente
				GameObject.Find ("ScrollPanel").GetComponent<ScrollRect>().horizontalNormalizedPosition = 0.12f;

				break;

			case 3:

				// Seta a posição do scroll rect no episódio selecionado anteriormente
				GameObject.Find ("ScrollPanel").GetComponent<ScrollRect>().horizontalNormalizedPosition = 0.27f;

				break;

			case 4:

				// Seta a posição do scroll rect no episódio selecionado anteriormente
				GameObject.Find ("ScrollPanel").GetComponent<ScrollRect>().horizontalNormalizedPosition = 0.43f;

				break;

			case 5:

				// Seta a posição do scroll rect no episódio selecionado anteriormente
				GameObject.Find ("ScrollPanel").GetComponent<ScrollRect>().horizontalNormalizedPosition = 0.59f;

				break;

			case 6:

				// Seta a posição do scroll rect no episódio selecionado anteriormente
				GameObject.Find ("ScrollPanel").GetComponent<ScrollRect>().horizontalNormalizedPosition = 0.74f;

				break;

			case 7:

				// Seta a posição do scroll rect no episódio selecionado anteriormente
				GameObject.Find ("ScrollPanel").GetComponent<ScrollRect>().horizontalNormalizedPosition = 0.9f;

				break;

			case 8:

				// Seta a posição do scroll rect no episódio selecionado anteriormente
				GameObject.Find ("ScrollPanel").GetComponent<ScrollRect>().horizontalNormalizedPosition = 1;

				break;

			case 9:

				// Seta a posição do scroll rect no episódio selecionado anteriormente
				GameObject.Find ("ScrollPanel").GetComponent<ScrollRect>().horizontalNormalizedPosition = 1;

				break;

			default:

				break;
			}
		}
	}

//	/// <summary>
//	/// Moves the scroll rect left.
//	/// </summary>
//	public void MoveScrollRectLeft ()
//	{
//		// Seta que o contador inicia em 0
//		i_scrollRectAnimationCounter = 0;
//
//		// Inicia a animacao de mover para a esquerda
//		StartCoroutine("AnimationMoveScroolRectLeft");
//
//		// Toca o som do botão
//		sfxController.SetPlayAudioButton(1.0f);
//	}
//
//	/// <summary>
//	/// Moves the scroll rect right.
//	/// </summary>
//	public void MoveScrollRectRight ()
//	{
//		// Seta que o contador inicia em 0
//		i_scrollRectAnimationCounter = 0;
//
//		// Inicia a animacao de mover para a direita
//		StartCoroutine("AnimationMoveScroolRectRight");
//
//		// Toca o som do botão
//		sfxController.SetPlayAudioButton(1.0f);
//	}
//
//	/// <summary>
//	/// Animations the move scrool rect right.
//	/// </summary>
//	/// <returns>The move scrool rect right.</returns>
//	IEnumerator AnimationMoveScroolRectRight ()
//	{
//		// Movimenta o scroll para a direita
//		scrollRect_categoryScrollSelectionPanel.horizontalNormalizedPosition += 0.0131f;
//
//		// Aguarda um tempo para movimentar mais
//		yield return new WaitForSeconds (0.01f);
//
//		// Incrementa o contador da animaçao
//		i_scrollRectAnimationCounter++;
//
//		// Verifica se ainda nao movimentou 10 vezes
//		if (i_scrollRectAnimationCounter < 10)
//		{
//			// Inicia a animacao de mover para a direita
//			StartCoroutine("AnimationMoveScroolRectRight");
//		}
//	}
//
//	/// <summary>
//	/// Animations the move scrool rect left.
//	/// </summary>
//	/// <returns>The move scrool rect left.</returns>
//	IEnumerator AnimationMoveScroolRectLeft ()
//	{
//		// Movimenta o scroll para a direita
//		scrollRect_categoryScrollSelectionPanel.horizontalNormalizedPosition -= 0.0131f;
//		
//		// Aguarda um tempo para movimentar mais
//		yield return new WaitForSeconds (0.01f);
//		
//		// Incrementa o contador da animaçao
//		i_scrollRectAnimationCounter++;
//		
//		// Verifica se ainda nao movimentou 10 vezes
//		if (i_scrollRectAnimationCounter < 10)
//		{
//			// Inicia a animacao de mover para a direita
//			StartCoroutine("AnimationMoveScroolRectLeft");
//		}
//	}
}