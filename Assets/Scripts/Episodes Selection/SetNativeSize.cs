﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// ****************
// EXECUTA IN EDIT MODE
//[ExecuteInEditMode]
public class SetNativeSize : MonoBehaviour 
{
	private Image img;

	// Use this for initialization
	void Start () 
	{
		img = this.GetComponent<Image>();
		img.SetNativeSize ();
	}

//	void Update () 
//	{
//		img = this.GetComponent<Image>();
//		img.SetNativeSize ();
//	}	

//	// Use this for initialization
//	public void RefreshNativeSize () 
//	{
//		//img = this.GetComponent<Image>();
//		img.SetNativeSize ();
//	}
}