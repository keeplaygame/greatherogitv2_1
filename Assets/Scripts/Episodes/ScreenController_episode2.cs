﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Reflection;
using UnityEngine.EventSystems;

//Para forçar salvar.
public class ScreenController_episode2 : MonoBehaviour 
{
	// Enumerador com todas as telas do jogo
	enum Screens
	{
		TelaFases,
		Modulo1,
		Modulo2,
		Modulo3,
		Modulo4,
		Modulo5,
		Modulo6,
		Modulo7,
		Modulo8,
		Modulo9,
		Modulo10
	}

	// Enumerador com os nomes dos selos dos episódios
	enum EpisodeStampName
	{
		Purity,
		Confidence,
		Humbleness,
		Devotion,
		Justice,
		Bravery,
		Obedience,
		Friendship,
		Kindness,
		Love
	}

	// Nome do selo do episódio
	private EpisodeStampName enum_episodeStampName;

	[Header("Largura e Altura da imagem de referência")]
	[SerializeField]
	// Largura de referencia (maior imagem)
	private float f_referenceWidth;
	
	[SerializeField]
	// Altura de referencia (maior imagem)
	private float f_referenceHeight;

	// Indica a tela atual do jogo
	private Screens enum_currentScreen; 

	// Guarda o game object da tela das fases
	private GameObject go_levels; // Camera da tela fases.

	[Header("Game Objects das telas das fases de 1 a 10")]
	[SerializeField]
	// Guarda os game objects das telas das fases de 1 a 10
	private GameObject[] array_go_level1to10 = new GameObject[10];

	// Indica se os botoes da tela estao ativos
	private bool b_isScreenButtonsActive; 

	// Recebe e guarda o  gameobject do canvas da scene
	private GameObject go_sceneCanvas;

	// Recebe e guarda o GameObject que tem o AudioController Script.
	private AudioController audioController;

	// Recebe e guarda o GameObject que tem o MusicController Script.
	private MusicController musicController;

	// Recebe o episode controller do episodio
	private EpisodeController episodeController;

	// Recebe o level controller
	private LevelControllerExModuloController levelController;

	// Recebe o controlador de compras
	private InAppController2 inAppController;

	void Awake ()
	{
		// Configuração do canvas
		CanvasConfiguration();

		// Pega todos os game objects que o level controller precisa
		GetGameObjectsAndComponents();

		// Checa o status do audio.
		audioController.CheckAudioStatus ();

		#if UNITY_IOS
		// Para o indicador de atividade
		LoadingController.HideActivity();
		#endif
	}

	// Use this for initialization
	void Start () 
	{
		// Inicia na tela de splash 01
		enum_currentScreen = Screens.TelaFases;

		// Pega qual e o selo do episódio
		enum_episodeStampName = (EpisodeStampName)episodeController.GetCurrentEpisode ();

		// Seta que os botoes da tela estao ativos.
		b_isScreenButtonsActive = true; 

		// EXECUTA função para o LANGUAGE.CS atualizar o idioma da tela. 
		Language.SwitchLanguage(Language.CurrentLanguage());
	}

	/// <summary>
	/// Determines whether this instance canvas configuration.
	/// </summary>
	/// <returns><c>true</c> if this instance canvas configuration; otherwise, <c>false</c>.</returns>
	private void CanvasConfiguration()
	{
		// Guarda o gameobject do canvas
		go_sceneCanvas = GameObject.Find("Canvas");
		
		// Guarda o canvas scaler do canvas
		CanvasScaler canvasScaler = go_sceneCanvas.GetComponent<CanvasScaler>();

		// AspectRatio Referencial
		float fReferenceAspectRatio;
		
		// Verifica se a largura é maior ou igual que a altura
		if (f_referenceWidth >= f_referenceHeight)
		{
			// Calculo do aspectRatio referencial
			fReferenceAspectRatio = (float)f_referenceWidth/f_referenceHeight;
		}
		
		// A altura é maior
		else
		{
			// Calculo do aspectRatio
			fReferenceAspectRatio = (float)f_referenceHeight/f_referenceWidth;
		}
		
		// Proporção do tamanho da tela
		float fAspectRatio;
		
		// Verifica se a largura é maior ou igual que a altura
		if (Screen.width >= Screen.height)
		{
			// Calculo do aspectRatio
			fAspectRatio = (float)Screen.width/Screen.height;
		}
		
		// A altura é maior
		else
		{
			// Calculo do aspectRatio
			fAspectRatio = (float)Screen.height/Screen.width;
		}
		
		// Verifica se o aspect ratio é menos wide que a referência
		if (fAspectRatio < fReferenceAspectRatio)
		{
			// Seta o valor do canvas scaler
			canvasScaler.matchWidthOrHeight = 1;
		}
		
		// Verifica se o aspect ratio é mais wide que a referência
		else if (fAspectRatio > fReferenceAspectRatio)
		{
			// Seta o valor do canvas scaler
			canvasScaler.matchWidthOrHeight = 0;
		} 
		
		// O aspect ratio é igual a referência
		else
		{
			// Seta o valor do canvas scaler
			canvasScaler.matchWidthOrHeight = 0.5f;
		}
	}

	/// <summary>
	/// Gets the game objects.
	/// </summary>
	private void GetGameObjectsAndComponents ()
	{
		// Guarda o game object do levels
		go_levels = GameObject.Find ("Levels");

		// Guarda o audio controller
		audioController = GameObject.Find("AudioController").GetComponent<AudioController>();

		// Guarda o music controller
		musicController = GameObject.Find("MusicController").GetComponent<MusicController>();

		// Guarda o episode controller
		episodeController = GameObject.Find("EpisodeController").GetComponent<EpisodeController>();

		// Guarda o level controller
		levelController = GameObject.Find("LevelController").GetComponent<LevelControllerExModuloController>();

		// Guarda o inApp controller
		inAppController = GameObject.Find("InAppController").GetComponent<InAppController2>();
	}

	/// <summary>
	/// Changes the screen.
	/// </summary>
	private void ChangeScreen ()
	{
		// Desabilita a camera de escolha de fases
		go_levels.SetActive(false);

		// Percorre todas as fases
		for (int i = 0; i < array_go_level1to10.Length; i++) 
		{
			// Desabilita a camera da fase
			array_go_level1to10[i].SetActive(false);
		}

		// Verifica se é a tela de fases
		if (enum_currentScreen == Screens.TelaFases) 
		{
			// Habilita a camera da tela de fases
			go_levels.SetActive(true);

			// Atualiza as fases abertas
			episodeController.UpdateLevels();

			// Verifica se deve iniciar a compra das fases
			if (PlayerPrefs.GetInt("iIsToStartLevelsPurchase", 0) == 1)
			{
				//
				// TROCAR quando for usar a conta oficial da Mount Moriah
				//
				
				#if UNITY_IPHONE
				// Inicia a compra das fases
				inAppController.PurchaseProduct(episodeController.GetStrEpisodeName() + "Levels");
				#endif
				
				// Deleta a chave que deve iniciar a compra das fases
				PlayerPrefs.DeleteKey("iIsToStartLevelsPurchase");
			}
		}

		// É a tela de uma das 10 fases
		else
		{
			// Habilita a câmera da fase
			array_go_level1to10[(int)enum_currentScreen-1].SetActive(true);

			// Reinicia os itens arrastáveis
			GameObject.Find("PanelDragItens").GetComponent<PanelDragController>().RestartDragItens();

			// Reinicia as sombras que recebem os itens arrastáveis
			GameObject.Find("PanelDropItens").GetComponent<PanelDropController>().RestartDropItens();
		}

		// EXECUTA função para o LANGUAGE.CS atualizar o idioma da tela. 
		Language.SwitchLanguage(Language.CurrentLanguage());

		// Seta que os botões estão ativos
		b_isScreenButtonsActive = true;
		
		// Descarrega os assets que não estão sendo usados
		Resources.UnloadUnusedAssets();
	}

	/// <summary>
	/// Finishs the level screen.
	/// </summary>
	public void FinishLevelScreen (int iLevel)
	{
		// Verifica se Não está na última fase
		if (iLevel != 9)
		{
			// Seta que a fase do episódio foi completada - A primeira fase é a 0
			episodeController.SetLevelCompleted(iLevel);
			
			// Seta que a tela é a da próxima fase
			enum_currentScreen = (Screens)iLevel+2;
			
			// Envia para o gameObject MusicController que é para tocar a musica de INGAME.
			musicController.SetMusic ("Episode");
			
			// Seta para iniciar a próxima fase
			levelController.setStartLevel (iLevel+1);

			// Verifica se está finalizando a quinta fase
			if (iLevel == 4)
			{
				// Verifica se as fases do episódio Não foram compradas (estão travadas)
				if (PlayerPrefs.GetInt("isEpisode" + episodeController.GetStrEpisodeName() + "LevelsUnLocked") != 1)
				{// Seta que a camera atual é da tela de fases.
					enum_currentScreen = Screens.TelaFases;
					
					// Seta que é para efetuar a compra das fases
					PlayerPrefs.SetInt("iIsToStartLevelsPurchase", 1);
				}
			}

			// Seta para mudar de tela
			ChangeScreen ();
		}

		// Está na última fase
		else 
		{
			// Reinicia as fases
			episodeController.RestartLevels ();

			// Guarda a quantidade de selos a se ganhar
			int iNewQuantity = PlayerPrefs.GetInt(enum_episodeStampName.ToString() + "NewQuantity", 0);

			// Incrementa 1 selo
			iNewQuantity++;

			// Guarda o valor dos selos a se ganhar
			PlayerPrefs.SetInt(enum_episodeStampName.ToString() + "NewQuantity", iNewQuantity);

			// Seta que deve mostrar a galeria de selos
			PlayerPrefs.SetInt ("iIsToShowStampGallery", 1);
			
			// CARREGA a scene MainScene.
			SceneController.LoadLevelMainScene ();
		}
	}

	/// <summary>
	/// Sets the screen buttons not active.
	/// </summary>
	public void SetScreenButtonsNotActive ()
	{
		// Seta que os botões da tela não estão ativos
		b_isScreenButtonsActive = false;
	}

	#region case void SetScreen

	/// <summary>
	/// Sets the level screen.
	/// </summary>
	/// <param name="currentLevel">Current level.</param>
	public void SetLevelScreen (int currentLevel)
	{
		// Toca o som de botao apertado
		audioController.SetPlaySFXAudio (0.3f);
		
		// Seta que a tela é a da fase selecionada
		enum_currentScreen = (Screens)currentLevel+1;

		// Envia para o gameObject MusicController que é para tocar a musica de INGAME.
		musicController.SetMusic ("Episode");

		// Seta para iniciar a fase atual
		levelController.setStartLevel (currentLevel);

		// Seta para mudar de tela
		ChangeScreen ();
	}

	/// <summary>
	/// Backs the screen.
	/// </summary>
	public void BackScreen ()
	{
		// Verifica se os botoes da tela estah ativos.
		if (b_isScreenButtonsActive) 
		{
			// Coloca a animação da partícula longe da visão da câmera
			GameObject.FindGameObjectWithTag ("CorrectParticle").transform.position = new Vector2(-500.0f, -500.0f);

			// Toca o som de botao apertado.
			audioController.SetPlaySFXAudio (0.3f);
			
			// Verifica se e a tela de fases
			if (enum_currentScreen == Screens.TelaFases) 
			{
				// Envia para o gameObject MusicController que eh para tocar a musica de SPLASH AND MAIN MENU.
				musicController.SetMusic ("SplashAndMainMenu");

				// Salva qual o episódio selecionado
				PlayerPrefs.SetInt("iLastEpisodeSelected", episodeController.GetCurrentEpisode());
				
				// CARREGA a scene MainScene.
				SceneController.LoadLevelMainScene ();
			}
			
			// É uma tela de uma fase
			else 
			{
				// Carrega a tela de FASES.
				enum_currentScreen = Screens.TelaFases; 

				// Envia para o gameObject MusicController que eh para tocar a musica de INGAME.
				musicController.SetMusic ("Episode");

				// Seta para mudar de tela
				ChangeScreen ();
			}
		}
	}
	
	/// <summary>
	/// Sets the screen.
	/// </summary>
	public void SetAction (string strActionName)
	{
		// Verifica se os botões da tela estão ativos.
		if (b_isScreenButtonsActive)
		{
			// Verifica qual eh o Nome da Acao.
			switch (strActionName)
			{
				// Caso seja o botão de NEXT para a tela da Galeria de Selos.
			case "StampsGallery":

				// Reinicia as fases
				episodeController.RestartLevels();

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Envia para o gameObject MusicController que eh para tocar a musica de SPLASH AND MAIN MENU.
				musicController.SetMusic ("SplashAndMainMenu");

				// Seta que deve mostrar a galeria de selos
				PlayerPrefs.SetInt("iIsToShowStampGallery", 1);

				// CARREGA a scene MainScene.
				SceneController.LoadLevelMainScene();

				break;

			default:
				
				break;
			}
		}
	}
	#endregion
}