﻿using UnityEngine;
using System.Collections;

// ****************
// EXECUTA IN EDIT M
//[ExecuteInEditMode]
public class GUITextureAspectRatioScale : MonoBehaviour 
{
	// Indica a escala em porcentagem do tamanho da tela, para uma tela quadrada
	private Vector2 vec2_scaleOnRatio1;

	// Guarda a textura do objeto
	private Texture texture_this;

	// Aspecto da largura pela altura
	private float f_widthHeightRatio;

	// Largura de referencia (maior resolucao)
	private float f_widthReference;

	// Altura de referencia (maior resolucao)
	private float f_heightReference;

	// Indica se a imagem deve manter o aspect ratio
	public bool b_isToMantainAspectRatio;

	void Start ()
	{
		// Seta a largura de referencia
		f_widthReference = Screen.width;
		
		// Seta a altura de referencia
		f_heightReference = Screen.height;
		
		// Pega o componete guiTexture
		texture_this = GetComponent<GUITexture>().texture;
		
		// Aplica a escala inicial
		this.transform.localScale = new Vector3 (texture_this.width/f_widthReference, texture_this.height/f_heightReference, 1); 
		
		// Calcula os valores de escala para aspecto 1
		//vec2_scaleOnRatio1 = new Vector2(texture_this.width/f_widthReference, texture_this.height/f_widthReference);
		vec2_scaleOnRatio1 = new Vector2(texture_this.width/f_heightReference, texture_this.height/f_heightReference);
		
		// Verifica se deve manter o aspect ratio
		if (b_isToMantainAspectRatio)
		{
			// Seta a escala do objeto
			SetScale();
		}
	}

	/// <summary>
	/// Sets the scale.
	/// </summary>
	void SetScale()
	{
		// Escontra o aspect ratio do dispositivo
		f_widthHeightRatio = (float)Screen.height/Screen.width;

		// Aplica a escala para manter as proporcoes
		this.transform.localScale = new Vector3 (vec2_scaleOnRatio1.x * f_widthHeightRatio, vec2_scaleOnRatio1.y, 1); 
	}
}
