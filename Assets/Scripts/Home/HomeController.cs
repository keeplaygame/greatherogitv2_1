﻿using UnityEngine;
using System.Collections;

public class HomeController : MonoBehaviour 
{
	// Recebe e guarda o GameObject que tem o botão de selos PT.
	public GameObject go_stampsBtnPT;

	// Recebe e guarda o GameObject que tem o botão de selos EN..
	public GameObject go_stampsBtnEN;

	/// <summary>
	/// Sets the stamps button.
	/// </summary>
	public void SetStampsBtn ()
	{
		// VERIFICA se o idioma selecionado é o EN.
		if(PlayerPrefs.GetInt("LanguageSelected") == 2)
		{
			// ATIVA o botão EN dos selos.
			go_stampsBtnEN.SetActive (true);

			// DESATIVA o botão PT dos selos.
			go_stampsBtnPT.SetActive (false);
		}

		// CASO seja o idioma PT.
		else
		{
			// ATIVA o botão PT dos selos.
			go_stampsBtnPT.SetActive (true);
			
			// DESATIVA o botão EN dos selos.
			go_stampsBtnEN.SetActive (false);
		}
	}
}