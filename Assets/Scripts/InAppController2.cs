using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31;

//Para forçar salvar.
public class InAppController2 : MonoBehaviourGUI
{
	// Recebe o episode controller
	private EpisodeController episodeController;

	// Indica se está fazendo uma compra
	private bool b_isPurchasing;

	void Awake ()
	{
		// Pega o episode controller
		episodeController = GameObject.Find("EpisodeController").GetComponent<EpisodeController>();
	}

	void Start ()
	{
		// Seta que não está efetuando uma compra
		b_isPurchasing = false;
	}

#if UNITY_IOS
	private List<StoreKitProduct> _products;

	void OnEnable()
	{
		// Escuta o evento de compra bem sucedida
		StoreKitManager.purchaseSuccessfulEvent += purchaseSuccessful;
		StoreKitManager.purchaseCancelledEvent += purchaseCancelledEvent;
		StoreKitManager.purchaseFailedEvent += purchaseFailedEvent;
//		StoreKitManager.productListReceivedEvent += productListReceivedEvent;
		StoreKitManager.productListRequestFailedEvent += productListRequestFailedEvent;
	}

	void OnDisable()
	{
		// Para de escutar o evento de compra bem sucedida
		StoreKitManager.purchaseSuccessfulEvent -= purchaseSuccessful;
		StoreKitManager.purchaseCancelledEvent += purchaseCancelledEvent;
		StoreKitManager.purchaseFailedEvent += purchaseFailedEvent;
//		StoreKitManager.productListReceivedEvent -= productListReceivedEvent;
		StoreKitManager.productListRequestFailedEvent -= productListRequestFailedEvent;
	}

	/// <summary>
	/// Requests the products.
	/// </summary>
	/// <param name="strproductIdentifier">Strproduct identifier.</param>
	private void RequestProducts (string strproductIdentifier)
	{
		Debug.Log( "requisitando");

		// you cannot make any purchases until you have retrieved the products from the server with the requestProductData method
		// we will store the products locally so that we will know what is purchaseable and when we can purchase the products.
		StoreKitManager.productListReceivedEvent += allProducts =>
		{
			Debug.Log( "received total products: " + allProducts.Count );
			_products = allProducts;

			// Seta que os produtos foram requisitados
			PlayerPrefs.SetInt("iIsProductsRequested", 1);

			// Verifica se está efetuando uma compra
			if (b_isPurchasing)
			{
				// Efetua a chamada de compra
				PurchaseProduct (strproductIdentifier);
			}
		};
		
		// array of product ID's from iTunesConnect. MUST match exactly what you have there!
		//var productIdentifiers = new string[] { "DavidAndGoliathLevels", "NoahsArkLevels" };
		var productIdentifiers = new string[] { "NoahsArkLevels", "JosephOfEgyptLevels", "MosesLevels", "JoshuaLevels", "DavidAndGoliathLevels", "JonahLevels", "DanielLevels", "TheBirthOfJesusLevels", "TheGreatHeroLevels" };
		StoreKitBinding.requestProductData( productIdentifiers );
	}

	/// <summary>
	/// Products the list request failed event.
	/// </summary>
	/// <param name="error">Error.</param>
	void productListRequestFailedEvent( string error )
	{
		Debug.Log( "productListRequestFailedEvent: " + error );
		
		// Esconde o indicador de atividade
		LoadingController.HideActivity();
		
		// Verifica se está efetuando uma compra
		if (b_isPurchasing)
		{
			// Verifica se o idioma é português
			if (PlayerPrefs.GetInt("LanguageSelected") == 1)
			{
				// Mensagem de restauração falhada
				EtceteraBinding.showAlertWithTitleMessageAndButton("Sem conexão de internet. Tente novamente mais tarde.", "", "OK");
			}
			
			// O idioma é o inglês
			else
			{
				// Mensagem de restauração falhada
				EtceteraBinding.showAlertWithTitleMessageAndButton("No internet access. Try again later.", "", "OK");
			}
		}
		
		// Seta que não precisa efetuar a compra
		b_isPurchasing = false;
	}

	/// <summary>
	/// Purchases the successful.
	/// </summary>
	/// <param name="transaction">Transaction.</param>
	void purchaseSuccessful( StoreKitTransaction transaction )
	{
		Debug.Log( "purchased product: " + transaction );

		// Seta que as fases do episodio estão destravadas
		PlayerPrefs.SetInt("isEpisode" + episodeController.GetStrEpisodeName() + "LevelsUnLocked", 1);

		// Avisa o levels controller que as fases foram compradas
		episodeController.PurchasedLevels();

		// Esconde o indicador de atividade
		LoadingController.HideActivity();
	}

	/// <summary>
	/// Purchases the failed event.
	/// </summary>
	/// <param name="error">Error.</param>
	void purchaseFailedEvent( string error )
	{
		Debug.Log( "purchaseFailedEvent: " + error );

		//
		// Mensagem sem conexão: Cannot connect to iTunes Store
		//

		// Esconde o indicador de atividade
		LoadingController.HideActivity();

		// Verifica se o idioma é português
		if (PlayerPrefs.GetInt("LanguageSelected") == 1)
		{
			// Mensagem de restauração falhada
			EtceteraBinding.showAlertWithTitleMessageAndButton("Não foi possível se conectar a iTunes.", "", "OK");
		}
		
		// O idioma é o inglês
		else
		{
			// Mensagem de restauração falhada
			EtceteraBinding.showAlertWithTitleMessageAndButton("Couldn't connect to iTunes.", "", "OK");
		}
	}
	
	/// <summary>
	/// Purchases the cancelled event.
	/// </summary>
	/// <param name="error">Error.</param>
	void purchaseCancelledEvent( string error )
	{
		Debug.Log( "purchaseCancelledEvent: " + error );

		// Esconde o indicador de atividade
		LoadingController.HideActivity();
	}
#endif

	/// <summary>
	/// Purchases the product.
	/// </summary>
	/// <param name="str_productIdentifier">Str_product identifier..</param>
	public void PurchaseProduct (string strproductIdentifier)
	{
		Debug.Log("Comprando produto: " + strproductIdentifier);

		#if UNITY_IOS
		// Mostra o indicador de atividade
		LoadingController.ShowActivity();
		
		// Verifica se os produtos foram requisitados
		if (PlayerPrefs.GetInt("iIsProductsRequested", 0) == 1)
		{
			// Seta que não está efetuando uma compra
			b_isPurchasing = false;
			
			// Efetua a chamada de compra
			StoreKitBinding.purchaseProduct (strproductIdentifier, 1);
		}
		
		// Os produtos não foram requisitados
		else
		{
			// Seta que está efetuando uma compra
			b_isPurchasing = true;
			
			// Requisita os produtos
			RequestProducts(strproductIdentifier);
		}
		#endif
	}

	/// <summary>
	/// Purchases the episode levels.
	/// </summary>
	public void PurchaseEpisodeLevels ()
	{
		// Guarda o nome do episódio
		string strproductIdentifier = episodeController.GetStrEpisodeName () + "Levels";

		Debug.Log("Comprando fases do episódio: " + strproductIdentifier);
		
		#if UNITY_IOS
		// Mostra o indicador de atividade
		LoadingController.ShowActivity();
		
		// Verifica se os produtos foram requisitados
		if (PlayerPrefs.GetInt("iIsProductsRequested", 0) == 1)
		{
			Debug.Log("Produtos foram requisitados.");

			// Seta que não está efetuando uma compra
			b_isPurchasing = false;
			
			// Efetua a chamada de compra
			StoreKitBinding.purchaseProduct (strproductIdentifier, 1);
		}
		
		// Os produtos não foram requisitados
		else
		{
			Debug.Log("Produtos NAO foram requisitados.");

			// Seta que está efetuando uma compra
			b_isPurchasing = true;
			
			// Requisita os produtos
			RequestProducts(strproductIdentifier);
		}
		#endif
	}
}
