﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Initial loading.
/// Esse script serve para identificar qual é o aspect ratio do device do usuário e encaminhá-lo para a scene correspondente ao aspect ratio identificado.
/// Esse script deve ser utilizado em uma scene que antecede as scenes do jogo propriamente dito, ou seja, ele deve estar em uma scene após a Splash obrigatória da Unity.
/// </summary>

public class InitialLoading : MonoBehaviour 
{
	// Enumerador que indica qual e o episódio
	private enum inAppVersion
	{
		MountMoriah,
		Raise
	}

	[Header("Versão Mount Moriah ou Raise")]
	[SerializeField]
	// Indica se existe inApps
	private inAppVersion enum_inAppVersion;

	void Start () 
	{	
		// Deleta a chave que indica que a splash já foi vista
		PlayerPrefs.DeleteKey("iIsSplashShowed");

		// Deleta a chave que indica o último episódio selecionado
		PlayerPrefs.DeleteKey("iLastEpisodeSelected");

		// Deleta a chave que indica que deve mostrar a galeria de selos
		PlayerPrefs.DeleteKey("iIsToShowStampGallery");

		// Deleta a chave que indica se o som está ligado
		PlayerPrefs.DeleteKey("iIsAudioOn");

		// Deleta a chave que indica que os produtos da loja foram guardados
		PlayerPrefs.DeleteKey("iIsProductsRequested");

		// Deleta a chave que indica que deve ser iniciada a compra das fases
		PlayerPrefs.DeleteKey("iIsToStartLevelsPurchase");

		// Deleta a chave que indica que o MasterSetupController ja foi inicializado.
		PlayerPrefs.DeleteKey("i_isFirstSetupMade");

		// Verifica se não tem inAPP (versão da Raise)
		if (enum_inAppVersion == inAppVersion.Raise) 
		{
			// Seta que todos espisódios foram comprados
			PlayerPrefs.SetInt("isEpisode" + "AdamAndEve" + "LevelsUnLocked", 1);
			PlayerPrefs.SetInt("isEpisode" + "NoahsArk" + "LevelsUnLocked", 1);
			PlayerPrefs.SetInt("isEpisode" + "JosephOfEgypt" + "LevelsUnLocked", 1);
			PlayerPrefs.SetInt("isEpisode" + "Moses" + "LevelsUnLocked", 1);
			PlayerPrefs.SetInt("isEpisode" + "Joshua" + "LevelsUnLocked", 1);
			PlayerPrefs.SetInt("isEpisode" + "DavidAndGoliath" + "LevelsUnLocked", 1);
			PlayerPrefs.SetInt("isEpisode" + "Jonah" + "LevelsUnLocked", 1);
			PlayerPrefs.SetInt("isEpisode" + "Daniel" + "LevelsUnLocked", 1);
			PlayerPrefs.SetInt("isEpisode" + "TheBirthOfJesus" + "LevelsUnLocked", 1);
			PlayerPrefs.SetInt("isEpisode" + "TheGreatHero" + "LevelsUnLocked", 1);
		}

		// Manda rodar função para setar qual cena carregar.
		SetScene();
	}	

	/// <summary>
	/// Sets the scene.
	/// </summary>
	private void SetScene ()
	{
		// Envia para a Main Scene
		SceneController.LoadLevelMainSceneWithoutShowActivity();
	}
}