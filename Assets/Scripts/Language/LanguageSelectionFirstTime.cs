﻿using UnityEngine;
using System.Collections;

//
// PlayerPrefs "LanguageSelected"
// 0 = Nenhum idioma selecionado ainda.
// 1 = Portuguese;
// 2 = English;
//

public class LanguageSelectionFirstTime : MonoBehaviour 
{
	// Recebe o screen controller da cena principal.
	public ScreenController_MainScene2 screenController_MainScene2;

	// Guarda o nome da proxima camera
	public string str_actionName;

	/// <summary>
	/// Sets the language selected.
	/// </summary>
	/// <param name="strLanguageSelected">String language selected.</param>
	public void SetLanguageSelected (string strLanguageSelected)
	{
		// Verifica a string do idioma selecionado
		switch (strLanguageSelected)
		{
		case("Portuguese"):

			// Grava no PlayerPrefs "LanguageSelected" que o idioma Portuguese foi selecionado.
			PlayerPrefs.SetInt("LanguageSelected", 1);

			// Seta o idioma para português
			Language.SwitchLanguage(LanguageCode.PT);

			break;

		case("English"):

			// Grava no PlayerPrefs "LanguageSelected" que o idioma English foi selecionado.
			PlayerPrefs.SetInt("LanguageSelected", 2);

			// Seta o idioma para ingles
			Language.SwitchLanguage(LanguageCode.EN);

			break;

		default:

			break;
		}

		// SALVA o PlayerPrefs.
		PlayerPrefs.Save ();

		// Envia para o screen qual camera deve ser habilitada
		screenController_MainScene2.SetAction(str_actionName);
	}
}