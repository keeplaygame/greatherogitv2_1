﻿using UnityEngine;
using System.Collections;
//Para forçar salvar.
public class LevelController2 : MonoBehaviour 
{
	// Recebe o nome do episódio
	public string str_episodeName;

	// Recebe os botoes das fases com estrela
	private GameObject[] array_go_levelButtonWithStars = new GameObject[10];

	// Recebe os botoes das fases cinzas
	private GameObject[] array_go_levelButtonGray = new GameObject[10];

	// Recebe os cadeados das fases travadas
	private GameObject[] array_go_levelPadlock = new GameObject[5];

	// Recebe o botão de compra do in app
	private GameObject go_buyButton;

	// Indica se as fases do episódio estão destravadas
	private bool b_isEpisodeLevelsUnLocked;

	// Indica se as fases foram completadas
	private bool[] array_b_isLevelsCompleted = new bool[10];

	void Awake ()
	{
		// Pega todos os game objects que o level controller precisa
		GetGameObjectsAndComponents();
	}

	void Start ()
	{
		// Percorre todas as fases
		for (int i = 0; i < array_b_isLevelsCompleted.Length; i++)
		{
			// Verifica se nao completou a fase do episódio
			if (PlayerPrefs.GetInt("isLevel" + str_episodeName + "_" + i + " Completed") == 0)
			{
				// Seta que nao completou a fase
				array_b_isLevelsCompleted[i] = false;
			}
			
			// Completou a fase
			else
			{
				// Seta que completou a fase
				array_b_isLevelsCompleted[i] = true;
			} 
		}

		// VERIFICA se o nome do episodio é AdamAndEve. 
		if(str_episodeName == "AdamAndEve")
		{
			// SETA que o playerPref que lida com as travas das fases do episódio AdamAndEve é igual a 1. Isso porque o episódio do Adão e Eva é totalmente
			// aberto para os usuários. É o único episódio que é totalmente gratuito.
			PlayerPrefs.SetInt("isEpisode" + str_episodeName + "LevelsUnLocked", 1);
		}

		// Verifica se as fases do episódio NÃO estão destravadas
		if (PlayerPrefs.GetInt("isEpisode" + str_episodeName + "LevelsUnLocked") == 0)
		{
			// Seta que as fases do episódio não estão destravadas
			b_isEpisodeLevelsUnLocked = false;
		}
		
		// As fases do episódio estão destravadas
		else
		{
			// Seta que as fases do episódio estão destravadas
			b_isEpisodeLevelsUnLocked = true;

			// Desativa o botão de compra
			go_buyButton.SetActive(false);
			
			// Percorre todos os cadeados
			for (int i = 0; i < array_go_levelPadlock.Length; i++)
			{
				// Desativa o cadeado
				array_go_levelPadlock[i].SetActive(false);
			}
		}

		// Atualiza as fases
		UpdateLevels();
	}

	/// <summary>
	/// Gets the name of the string episode.
	/// </summary>
	/// <returns>The string episode name.</returns>
	public string GetStrEpisodeName ()
	{
		// Retorna o nome do episódio
		return str_episodeName;
	}

	/// <summary>
	/// Updates the levels.
	/// </summary>
	public void UpdateLevels ()
	{
		// Percorre todas as fases
		for (int i = 0; i < array_b_isLevelsCompleted.Length; i++)
		{
			// Verifica se a fase foi completada
			if (array_b_isLevelsCompleted[i])
		    {
				// Ativa o botao com estrelas
				array_go_levelButtonWithStars[i].SetActive(true);

				// Verifica se as fases do episódio estão destravadas
				if (b_isEpisodeLevelsUnLocked)
				{
					// Verifica se não é a última fase que foi completada
					if (i < 9)
					{
						// Desativa o botao cinza da próxima fase
						array_go_levelButtonGray[i+1].SetActive(false);
					}
					
					// A última fase foi completada
					else
					{
						// Reinicia as fases
						RestartLevels();
					}
				}

				// As fases não estão destravadas
				else
				{
					// Verifica se não chegou na última fase liberada que
					if (i < 4)
					{
						// Desativa o botao cinza da próxima fase
						array_go_levelButtonGray[i+1].SetActive(false);
					}
				}
			}

			// A fase nao foi completada
			else
			{
				// Termina o for
				return;
			}
		}
	}

	/// <summary>
	/// Restarts the levels.
	/// </summary>
	public void RestartLevels ()
	{
		// Percorre todas as fases
		for (int i = 0; i < array_b_isLevelsCompleted.Length; i++)
		{
			// Seta que nao completou a fase do episódio
			PlayerPrefs.SetInt("isLevel" + str_episodeName + "_" + i + " Completed", 0);
			
			// Seta que nao completou a fase
			array_b_isLevelsCompleted[i] = false;
		}
	}

	/// <summary>
	/// Purchaseds the levels.
	/// </summary>
	public void PurchasedLevels ()
	{
		// Seta que as fases do episodio estao destravadas
		b_isEpisodeLevelsUnLocked = true;

		// Desativa o botão de compra
		go_buyButton.SetActive(false);

		// Percorre todos os cadeados
		for (int i = 0; i < array_go_levelPadlock.Length; i++)
		{
			// Desativa o cadeado
			array_go_levelPadlock[i].SetActive(false);
		}

		// Atualiza as fases
		UpdateLevels();
	}
	
	/// <summary>
	/// Sets the level completed.
	/// </summary>
	public void SetLevelCompleted (int iLevel)
	{
		// Grava que a fase foi completada
		PlayerPrefs.SetInt("isLevel" + str_episodeName + "_" + iLevel + " Completed", 1);

		// Seta que a fase foi completada
		array_b_isLevelsCompleted[iLevel] = true;
	}

	/// <summary>
	/// Gets the game objects.
	/// </summary>
	private void GetGameObjectsAndComponents ()
	{
		// Percorre todos os botões das 10 fases
		for (int i = 0; i < 10; i++)
		{
			// Guarda o game object do botão com estrelas
			array_go_levelButtonWithStars[i] = GameObject.Find((i+1).ToString() + "_com_estrelas");

			// Desativa o game object do botao com estrelas
			array_go_levelButtonWithStars[i].SetActive(false);
			
			// Verifica se não é o primeiro botão
			if (i > 0)
			{
				// Guarda o game object do botão cinza
				array_go_levelButtonGray[i] = GameObject.Find((i+1).ToString() + "_gray");
			}
			
			// Verifica se já percorreu 5 vezes
			if (i < 5)
			{
				// Guarda o game object do cadeado da fase
				array_go_levelPadlock[i] = GameObject.Find((i+6).ToString() + "_lock");
			}
		}
		
		// Guarda o botão de compra
		go_buyButton = GameObject.Find("BuyBtn");
	}
}