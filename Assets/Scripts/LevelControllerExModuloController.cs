﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelControllerExModuloController : MonoBehaviour 
{
	// Enumerador com todas as telas do jogo
	enum Levels
	{
		Level1,
		Level2,
		Level3,
		Level4,
		Level5,
		Level6,
		Level7,
		Level8,
		Level9,
		Level10
	}

	// Indica a fase atual
	private Levels enum_currentLevel;

	// Guarda o screen controller do jogo
	private ScreenController_episode2 screenController_episode;

	// Guarda o episode controller
	private EpisodeController episodeController;

	// Recebe e guarda o GameObject que tem o MusicController Script.
	private MusicController musicController;

	// Guarda o gameObject da main camera.
	[SerializeField]
	private GameObject go_mainCamera;

	// Guarda o gameObject da secondary camera.
	[SerializeField]
	private GameObject go_secondaryCamera;

	// Guarda o gameObject que tem a barra de interface.
	private GameObject go_interfaceBarBackground;

	// Guarda o gameObject que tem a barra lateral
	private GameObject go_lateralBar;

	// Guarda o game object do feedback normal.
	[SerializeField]
	private GameObject go_feedbackNormal;

	// Guarda o game object do feedback de selos.
	[SerializeField]
	private GameObject go_feedbackStamp;

	// Indica quantos itens estão no lugar certo
	private int i_itensAtRightPlace;

	// Define o ImageEffectsController.
	private ImageEffectsController imageEffectsController;

	// Define inteiro para qual imageEffect deve ser usado.
	// 0 = Grayscale Effect;
	// 1 = Blur Effect;
	private int i_imageEffect;

	private int i_isToPlayVictoryMusicAtFeedbackLevels1to9;

	void Awake ()
	{
		// Pega todos os game objects que o level controller precisa
		GetGameObjectsAndComponents();
	}

	// Use this for initialization
	void Start () 
	{
		// Seta que começa na primeira fase
		enum_currentLevel = Levels.Level1;

		// Seta que começa com 0 itens no lugar certo
		i_itensAtRightPlace = 0;

		// Seta qual o int do ImageEffect.
		i_imageEffect = PlayerPrefs.GetInt("ImageEffect");

		// Seta o script do imageEffectsController.
		imageEffectsController = go_mainCamera.GetComponent<ImageEffectsController>();

		// Seta qual o int do se é para tocar a feedback musica de vitoria nos levels 1-9.
		i_isToPlayVictoryMusicAtFeedbackLevels1to9 = PlayerPrefs.GetInt("i_isToPlayVictoryMusicAtFeedbackLevels1to9");
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="LevelControllerExModuloController"/> class.
	/// </summary>
	/// <param name="currentLevel">Current level.</param>
	public void  setStartLevel(int currentLevel)
	{
		// Seta a fase atual
		enum_currentLevel = (Levels)currentLevel;

		// Seta que começa com 0 itens no lugar certo
		i_itensAtRightPlace = 0;
	}

	/// <summary>
	/// Sets the right item.
	/// </summary>
	public void SetRightItem ()
	{
		// Adiciona um item na posição correta
		i_itensAtRightPlace++;

		// Verifica se é o último episódio e se é a última fase
		if (episodeController.GetCurrentEpisode() == 9 && enum_currentLevel == Levels.Level10) 
		{
			// Verifica se tem 1 item na posição correta
			if (i_itensAtRightPlace == 1) 
			{
				// Seta que Completou a fase
				isVictory ();
			}
		} 

		// É o último episódio e a última fase
		else 
		{
			// Verifica se tem 4 itens na posição correta
			if (i_itensAtRightPlace == 4) 
			{
				// Seta que Completou a fase
				isVictory ();
			}
		}
	}

	/// <summary>
	/// Gets the game objects.
	/// </summary>
	private void GetGameObjectsAndComponents ()
	{
		// Desativa o game objeto da secondary camera.
		go_secondaryCamera.SetActive (false);

		// Guarda o objeto da interface bar background.
		go_interfaceBarBackground = GameObject.Find("InterfaceBarBackgroundEpisodes");

		// Desativa o game objeto
		go_feedbackNormal.SetActive (false);

		// Desativa o game objeto
		go_feedbackStamp.SetActive (false);

		// Guarda o screen controller episode
		screenController_episode = GameObject.Find ("ScreenControllerEpisode").GetComponent<ScreenController_episode2>();

		// Guarda o episode controller
		episodeController = GameObject.Find ("EpisodeController").GetComponent<EpisodeController>();
		
		// Guarda o music controller
		musicController = GameObject.Find("MusicController").GetComponent<MusicController>();
	}
	
	/// <summary>
	/// Ises the victory.
	/// </summary>
	private void isVictory()
	{
		// Inicia a co-rotina para encerrar a fase
		StartCoroutine(WaitTimeToFinishModulo());
	}

	/// <summary>
	/// Waits the time to finish modulo.
	/// </summary>
	/// <returns>The time to finish modulo.</returns>
	IEnumerator WaitTimeToFinishModulo()
	{
		// Desabilita os botoes da tela
		screenController_episode.SetScreenButtonsNotActive();

		// Aguarda determinado tempo para encerrar a fase
		yield return new WaitForSeconds (0.5f);

		// DESATIVA a barra de interface.
		go_interfaceBarBackground.SetActive (false);

		// ATIVA o image effect para o feedback.
		ActivateImageEffectForFeedback ();

		// DESATIVA o componente Image do game object da lateral bar.
		go_lateralBar = GameObject.Find("LateralBar");

		// Pega o componente de image da barra lateral.
		Image co_imageLateralBar = go_lateralBar.GetComponent<Image>();
				
		// DESATIVA o componente image.
		co_imageLateralBar.enabled = false;

		// Verifica se não está na fase 10
		if (enum_currentLevel != Levels.Level10) 
		{
			// Verifica se é para tocar a música de vitória no feedback nos levels de 1 a 9.
			if(i_isToPlayVictoryMusicAtFeedbackLevels1to9 == 1) {
				// Envia para o GameObject MusicController que eh para tocar a musica de VITORIA.
				musicController.SetMusic ("Victory");
			}
			// Ativa o feedback normal
			go_feedbackNormal.SetActive (true);

			// Ativa a secondary camera.
			go_secondaryCamera.SetActive (true);
		}

		// Está na fase 10
		else 
		{
			// Envia para o GameObject MusicController que é para tocar a música de VITORIA.
			musicController.SetMusic ("Victory");

			// Ativa o feedback de selos
			go_feedbackStamp.SetActive(true);

			// Ativa a secondary camera.
			go_secondaryCamera.SetActive (true);
		}

		// Aguarda determinado tempo para encerrar o feedback
		yield return new WaitForSeconds (3.5f);

		// Desativa o feedback normal
		go_feedbackNormal.SetActive (false);

		// Verifica se não é a última fase
		if ((int)enum_currentLevel != 9) 
		{
			// DESATIVA a secondary camera.
			go_secondaryCamera.SetActive (false);

			// DESATIVA os image effects da main camera.
			imageEffectsController.SetImageEffectStatus("AllImageEffects", false);

			// ATIVA a barra de interface.
			go_interfaceBarBackground.SetActive (true);

			// ATIVA o componente image.
			co_imageLateralBar.enabled = true;
		}
	
		// Finaliza a tela atual e vai para a próxima
		screenController_episode.FinishLevelScreen((int)enum_currentLevel);
	}

	private void ActivateImageEffectForFeedback () {

		// Verifica se o int é igual a 0 (Grayscale Effect)
		if(i_imageEffect == 0) {
			// Ativa o efeito de grayscale.
			imageEffectsController.SetImageEffectStatus("GrayscaleEffect", true);
		} else {
			// Ativa o efeito de blur.
			imageEffectsController.SetImageEffectStatus("BlurEffect", true);	
		}
	}

	/// <summary>
	/// Gets the current level.
	/// </summary>
	/// <returns>The current level.</returns>
	public int GetCurrentLevel ()
	{
		// Retorna a fase atual
		return (int)enum_currentLevel;
	}
}