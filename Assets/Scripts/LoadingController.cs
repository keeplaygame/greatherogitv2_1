﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingController : MonoBehaviour 
{

	/// <summary>
	/// Shows the activity.
	/// </summary>
	public static void ShowActivity ()
	{
		// Habilita a imagem de background de carregar
		GameObject.FindGameObjectWithTag("LoadingBackgroundImage").GetComponent<Image>().enabled = true;

		#if UNITY_IPHONE
		
			// Seta para o indicador ser branco e grande
			Handheld.SetActivityIndicatorStyle(iOSActivityIndicatorStyle.WhiteLarge);
			
			// Inicia o indicador de atividade
			Handheld.StartActivityIndicator();

		#endif
	}

	/// <summary>
	/// Hides the activity.
	/// </summary>
	public static void HideActivity ()
	{
		// Desabilita a imagem de background de carregar
		GameObject.FindGameObjectWithTag("LoadingBackgroundImage").GetComponent<Image>().enabled = false;

		#if UNITY_IPHONE

			// Para o indicador de atividade
			Handheld.StopActivityIndicator();

		#endif
	}
}
