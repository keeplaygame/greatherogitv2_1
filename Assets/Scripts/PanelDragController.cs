﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelDragController : MonoBehaviour 
{
	// Total da altura de todos os arrastaveis
	private float f_totalDragsHeight;

	// Guarda a altura de cada arrastável
	private float[] array_f_dragHeight = new float[4];

	// Guarda a altura de cada arrastável em porcentagem referente a referência
	private float[] array_f_dragHeightPercentagem = new float[4];

	// Fator de escalonamento
	private float f_scalingFactor;

	// Espaço das bordas superior e inferior
	private float f_topAndBotSpaces = 0.05f;

	// Espaço das bordas esquerda e direita
	private float f_leftAndRightSpaces = 0.08f;

	// Espaço entre os arrastáveis
	private float f_dragSpaces = 0.05f;

	// Largura disponível
	private float f_availableWidth;

	/// <summary>
	/// Calculates the size of the drags position and.
	/// </summary>
	public void CalculateDragsPositionAndSize (Vector2 vec2_barDeltaSize)
	{
		// Calcula a porcentagem disponível para os arrastáveis
		float fAvailablePercentagemDragsHeight = 1 - (f_topAndBotSpaces * 2) - (f_dragSpaces * 3);

		// Calcula a largura disponível
		f_availableWidth = (1 - (f_leftAndRightSpaces * 2)) * vec2_barDeltaSize.x;

		// Inicia com 0
		f_totalDragsHeight = 0;

		// Indica a posiçao na barra do objeto arrastavel
		int iDragPosition = 0;
		
		// Para cada transform nesse transform
		foreach (Transform child in transform)
		{
			// Guarda o componente rectTransform do filho
			RectTransform rectChildImage = child.gameObject.GetComponent<RectTransform>();

			// Verifica se a largura da imagem é maior que a largura da barra lateral, considerando os espaços
			if (rectChildImage.sizeDelta.x > f_availableWidth)
			{
				// Calcula o fator de escala de x
				float fXScalingFactor = ((1-(f_leftAndRightSpaces*2))*vec2_barDeltaSize.x)/rectChildImage.sizeDelta.x;

				// Aplica o fator de escala
				child.gameObject.GetComponent<RectTransform>().localScale = new Vector3(fXScalingFactor, fXScalingFactor, fXScalingFactor);
			}
			
			// Guarda a altura do arrastável
			array_f_dragHeight[iDragPosition] = rectChildImage.sizeDelta.y * child.gameObject.GetComponent<RectTransform>().localScale.y;

			// Soma a altura ao total
			f_totalDragsHeight += rectChildImage.sizeDelta.y * child.gameObject.GetComponent<RectTransform>().localScale.y;
			
			// Vai para o próximo arrastável
			iDragPosition++;
		}

		// Altura total em porcentagem
		float fTotalheightPercentagem = 0;

		// Percorre todos os arrastáveis
		for (int i = 0; i < array_f_dragHeight.Length; i++)
		{
			// Calcula a altura em porcentagem do arrastável
			array_f_dragHeightPercentagem[i] = array_f_dragHeight[i]/vec2_barDeltaSize.y;

			// Soma a altura ao total
			fTotalheightPercentagem += array_f_dragHeightPercentagem[i];
		}

		// Divide a área total disponível para os arrastáveis, pela porcentagem ocupado pelos arrastáveis, para se ter o fator de escalonamento
		f_scalingFactor = fAvailablePercentagemDragsHeight/fTotalheightPercentagem;

		// Percorre todos os arrastáveis
		for (int i = 0; i < array_f_dragHeight.Length; i++)
		{
			// Aplica o fator de escala a porcentagem de altura
			array_f_dragHeightPercentagem[i] *= f_scalingFactor;
		}

		// Reseta o contador
		iDragPosition = 0;
		
		// Para cada transform nesse transform
		foreach (Transform child in transform)
		{
			// Arruma a posição e tamanho do objeto arrastável
			child.gameObject.GetComponent<DragMe>().UpdateSizeAndPosition(iDragPosition, array_f_dragHeightPercentagem, f_scalingFactor, f_topAndBotSpaces, f_dragSpaces, f_availableWidth);
			
			// Incrementa a posição para o próximo arrastável
			iDragPosition++;
		}
	}

	/// <summary>
	/// Restarts the drag itens.
	/// </summary>
	public void RestartDragItens ()
	{
		// Para cada transform nesse transform
		foreach (Transform child in transform)
		{
			// Ativa os itens de arrastar
			child.gameObject.SetActive(true);

			// Guarda o componente imagem do filho
			Image imgChildImage = child.gameObject.GetComponent<Image> ();

			// Coloca a cor do objeto arrastavel para opaco
			imgChildImage.color = new Color(1, 1, 1, 1);
		}
	}
}
