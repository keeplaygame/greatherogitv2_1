﻿using UnityEngine;
using System.Collections;

public class PanelDropController : MonoBehaviour 
{
	/// <summary>
	/// Restarts the drop itens.
	/// </summary>
	public void RestartDropItens ()
	{
		// Para cada transform nesse transform
		foreach (Transform child in transform)
		{
			// Volta o sprite original da sombra
			child.gameObject.GetComponent<DropMe>().receivingImage.overrideSprite = child.gameObject.GetComponent<DropMe>().receivingImage.sprite;
		}
	}
}
