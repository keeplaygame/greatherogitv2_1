﻿using UnityEngine;
using System.Collections;

public class ParticleSortLayer : MonoBehaviour 
{
	[SerializeField]
	// Nome da bool que indica se é para a partícula ser destruida.
	private bool b_isToDestroy;

	// Use this for initialization
	void Start () 
	{
		//Change Foreground to the layer you want it to display on 
		//You could prob. make a public variable for this
		particleSystem.renderer.sortingLayerName = "Particle";

		// VERIFICA se é para destroir a partícula.
		if(b_isToDestroy)
		{
			// Inicia a co-rotina para destruir a particula
			StartCoroutine(DestroyAfterEffect());
		}
	}

	/// <summary>
	/// Destroy the particle after effect.
	/// </summary>
	/// <returns>The after effect.</returns>
	IEnumerator DestroyAfterEffect ()
	{
		// Aguarda X segundos.
		yield return new WaitForSeconds(1.8f);

		// Destroi a partícula.
		Destroy(this.gameObject);
	}
}