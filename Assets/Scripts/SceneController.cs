﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour 
{
	/// <summary>
	/// Loads the level main scene without show activity.
	/// </summary>
	public static void LoadLevelMainSceneWithoutShowActivity ()
	{
		// Carrega a main scene
		Application.LoadLevel("1_MainScene");

		// TIRA da memória assets que não estão sendo utilizados.
		Resources.UnloadUnusedAssets ();
	}

	/// <summary>
	/// Loads the level main scene.
	/// </summary>
	public static void LoadLevelMainScene ()
	{
		// Mostra que está tendo atividade
		LoadingController.ShowActivity();

		// Carrega a main scene
		Application.LoadLevel("1_MainScene");

		// TIRA da memória assets que não estão sendo utilizados.
		Resources.UnloadUnusedAssets ();
	}

	/// <summary>
	/// Loads the level episode.
	/// </summary>
	/// <param name="strEpisode">String episode.</param>
	public static void LoadLevelEpisode (string strEpisode)
	{
		// Mostra que está tendo atividade
		LoadingController.ShowActivity();
		
		// Carrega a Scene do episódio.
		Application.LoadLevel(strEpisode);
		
		// TIRA da memória assets que não estão sendo utilizados.
		Resources.UnloadUnusedAssets ();
	}
}
