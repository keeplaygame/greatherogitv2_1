﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Prime31;

public class ScreenController_MainScene2 : MonoBehaviour 
{
	// Enumerador com todas as telas do jogo
	enum Screens
	{
		Splash01,
		Splash02,
		LanguageSelection,
		Home,
		EpisodesSelection,
		Creditos,
		Options,
		Stamps,
	}

	[Header("Largura e Altura da imagem de referência")]
	[SerializeField]
	// Largura de referencia (maior imagem)
	private float f_referenceWidth;
	
	[SerializeField]
	// Altura de referencia (maior imagem)
	private float f_referenceHeight;
	
	// Indica a tela atual do jogo
	private Screens enum_currentScreen; 

	// Decorator Drawer.
	[Header ("Cameras")]

	//
	// Telas Inicio.
	//
	public GameObject go_splash01; // Tela de splash 01
	public GameObject go_splash02; // Tela de splash 02
	public GameObject go_languageSelection; // Tela de selecao de idioma
	public GameObject go_home; // Tela de home
	public GameObject go_episodesSelection; // Tela da tela de home
	public GameObject go_creditos; // Tela de credito
	public GameObject go_options; // Tela de OPÇÕES.
	public GameObject go_stamps; // Tela dos SELOS.
	//
	// Telas Fim.
	//

	// Decorator Drawer.
	[Header ("Barra Interface")]

	[SerializeField]
	// Recebe a barra de interface.
	private GameObject go_interfaceBarBackground;

	[SerializeField]
	// Recebe o botao de back.
	private GameObject go_backButton;

	[SerializeField]
	// Recebe o botao de info.
	private GameObject go_infoButton;

	// Indica o idioma selecionado
	private int i_languageSelected;

	// Indica se os botoes da tela estao ativos.
	public bool b_isScreenButtonsActive;

	// Recebe e guarda o  gameobject do canvas da scene
	private GameObject go_sceneCanvas;

	// Indica se a Splash foi vista.
	private int i_isSplashShowed;

	// Indica se deve mostrar a galeria de selos
	private int i_isToShowStampGallery;

	// Recebe e guarda o stampGalleryController
	private StampGalleryController stampGalleryController;

	// Recebe e guarda EpisodesSelection Script.
	private EpisodesSelection episodesSelection;

	// Recebe e guarda o GameObject que tem o AudioController Script.
	private AudioController audioController;

	// Recebe e guarda o HomeController.
	private HomeController homeController;

	//Needs to be static.
	private static bool spawned = false;

	void Awake ()
	{
		// Configuração do canvas
		CanvasConfiguration();
		
		// Pega todos os game objects que o level controller precisa
		GetGameObjectsAndComponents();
		
		// Checa o status do audio.
		audioController.CheckAudioStatus ();

		#if UNITY_IOS
		// Para o indicador de atividade
		LoadingController.HideActivity();
		#endif
	}

	// Use this for initialization
	void Start () 
	{
		// Inicia na tela de splash 01
		enum_currentScreen = Screens.Splash01; 

		// Seta que nao estah mudando de tela.
		//b_isChangingScreen = false;

		// Seta que os botoes da tela estao ativos.
		b_isScreenButtonsActive = true;

		// Seta que a principio a splash nao foi mostrada
		i_isSplashShowed = PlayerPrefs.GetInt("iIsSplashShowed");

		// Seta que nenhum idioma foi selecionado a principio
		i_languageSelected = 0;

		// Inicia a corotina do tempo das telas de splash
		StartCoroutine(SplashScreenTimer());
	}
	
	/// <summary>
	/// Determines whether this instance canvas configuration.
	/// </summary>
	/// <returns><c>true</c> if this instance canvas configuration; otherwise, <c>false</c>.</returns>
	private void CanvasConfiguration()
	{
		// Guarda o gameobject do canvas
		go_sceneCanvas = GameObject.Find("Canvas");
		
		// Guarda o canvas scaler do canvas
		CanvasScaler canvasScaler = go_sceneCanvas.GetComponent<CanvasScaler>();
		
		// AspectRatio Referencial
		float fReferenceAspectRatio;
		
		// Verifica se a largura é maior ou igual que a altura
		if (f_referenceWidth >= f_referenceHeight)
		{
			// Calculo do aspectRatio referencial
			fReferenceAspectRatio = (float)f_referenceWidth/f_referenceHeight;
		}
		
		// A altura é maior
		else
		{
			// Calculo do aspectRatio
			fReferenceAspectRatio = (float)f_referenceHeight/f_referenceWidth;
		}
		
		// Proporção do tamanho da tela
		float fAspectRatio;
		
		// Verifica se a largura é maior ou igual que a altura
		if (Screen.width >= Screen.height)
		{
			// Calculo do aspectRatio
			fAspectRatio = (float)Screen.width/Screen.height;
		}
		
		// A altura é maior
		else
		{
			// Calculo do aspectRatio
			fAspectRatio = (float)Screen.height/Screen.width;
		}
		
		// Verifica se o aspect ratio é menos wide que a referência
		if (fAspectRatio < fReferenceAspectRatio)
		{
			// Seta o valor do canvas scaler
			canvasScaler.matchWidthOrHeight = 1;
		}
		
		// Verifica se o aspect ratio é mais wide que a referência
		else if (fAspectRatio > fReferenceAspectRatio)
		{
			// Seta o valor do canvas scaler
			canvasScaler.matchWidthOrHeight = 0;
		} 
		
		// O aspect ratio é igual a referência
		else
		{
			// Seta o valor do canvas scaler
			canvasScaler.matchWidthOrHeight = 0.5f;
		}
	}

	/// <summary>
	/// Gets the game objects.
	/// </summary>
	private void GetGameObjectsAndComponents ()
	{
		// Guarda o stamp gallery controller
		stampGalleryController = GameObject.Find ("StampGalleryController").GetComponent<StampGalleryController>();

		// Guarda o episodes controller
		episodesSelection = GameObject.Find ("EpisodesSelectionScript").GetComponent<EpisodesSelection>();
 
		// Guarda o audio controller
		audioController = GameObject.Find ("AudioController").GetComponent<AudioController>();

		// Guarda o home controller
		homeController = GameObject.Find ("HomeController").GetComponent<HomeController>();
	}

	/// <summary>
	/// Changes the screen.
	/// </summary>
	void ChangeScreen ()
	{
		// Desabilita todas as telas.
		go_splash01.SetActive(false);
		go_splash02.SetActive(false);
		go_languageSelection.SetActive(false);
		go_home.SetActive(false);
		go_episodesSelection.SetActive(false);
		go_creditos.SetActive(false);
		go_options.SetActive(false);
		go_stamps.SetActive(false);

		// Verifica qual eh a tela atual
		switch (enum_currentScreen)
		{
			// Caso seja a tela de splash 01
		case Screens.Splash01:

			// Habilita a camera de splash 01
			go_splash01.SetActive(true);

			break;

			// Caso seja a tela de splash 02
		case Screens.Splash02:

			// Habilita a camera de splash 02
			go_splash02.SetActive(true);

			break;

		case Screens.LanguageSelection:

			// Habilita a camera de selecao de idioma
			go_languageSelection.SetActive(true);

			break;

			// Caso seja a tela de home
		case Screens.Home:

			// Habilita a camera da tela de home
			go_home.SetActive(true);

			// ATIVA barra.
			go_interfaceBarBackground.SetActive(true);

			// Ativa o botao info e desativa o botao back.
			SetInterfaceBar("info");

			// Verifica qual status do audio para determinar qual sprite mostrar.
			audioController.CheckAudioStatus();
			
			// Executa funçao para definir qual botao mostrar de acordo com o idioma selecionado.
			homeController.SetStampsBtn();

			break;

			// Caso seja a tela de SELEÇÃO DE EPISÓDIOS.
		case Screens.EpisodesSelection:
			
			// Habilita a camera da tela de SELEÇÃO DE EPISÓDIOS.
			go_episodesSelection.SetActive(true);

			// ATIVA barra.
			go_interfaceBarBackground.SetActive(true);

			// Começa do último episódio selecionado
			episodesSelection.StartAtLastEpisodeSelected();

			// Verifica qual status do audio para determinar qual sprite mostrar.
			audioController.CheckAudioStatus();
			
			break;

			// Caso seja a tela de creditos
		case Screens.Creditos:

			// Habilita a tela de creditos
			go_creditos.SetActive(true);

			break;

			// Caso seja a tela de OPÇÕES.
		case Screens.Options:
			
			// Habilita a tela de OPÇÕES.
			go_options.SetActive(true);
			
			break;

			// Caso seja a tela dos SELOS.
		case Screens.Stamps:
			
			// Habilita a tela de SELOS.
			go_stamps.SetActive(true);

			// ATIVA barra.
			go_interfaceBarBackground.SetActive(true);

			// Reinica a galeria de selos
			stampGalleryController.RestartStampGallery();
			
			break;

		default:
			
			break;
		}

		// EXECUTA função para o LANGUAGE.CS atualizar o idioma da tela. 
		Language.SwitchLanguage(Language.CurrentLanguage());

		// Seta que os botoes estah ativos
		b_isScreenButtonsActive = true;

		// Usar aqui?? Mas não é assincrono?
		Resources.UnloadUnusedAssets();
	}

	/// <summary>
	/// Sets the screen buttons not active.
	/// </summary>
	public void SetScreenButtonsNotActive ()
	{
		// Seta que os botoes da tela nao estao ativos
		b_isScreenButtonsActive = false;
	}

	/// <summary>
	/// Splash screen timer.
	/// </summary>
	/// <returns>The screen timer.</returns>
	IEnumerator SplashScreenTimer ()
	{
		// Verifica se ainda nao mostrou a splash
		if (i_isSplashShowed == 0)
		{
			// Seta o tempo da tela de splash
			float fSplashTime = 1.5f;

			// Aguarda o tempo para mudar de tela
			yield return new WaitForSeconds (fSplashTime);

			// Carrega a tela de splash 02
			enum_currentScreen = Screens.Splash02;

			// Muda a tela
			ChangeScreen();

			// Aguarda o tempo para mudar de tela
			yield return new WaitForSeconds (fSplashTime);

			// Grava que já mostrou a splash
			PlayerPrefs.SetInt("iIsSplashShowed", 1);
			
			// Seta que a splash foi mostrada
			i_isSplashShowed = 1;

			// Carrega o idioma selecionado
			LoadLanguage();
		}

		// Já mostrou a splash
		else
		{
			// Verifica se não é para mostrar a galeria de selos
			if (PlayerPrefs.GetInt("iIsToShowStampGallery", 0) == 0)
			{
				// Carrega a tela de seleção de episódios
				enum_currentScreen = Screens.EpisodesSelection;
			}

			// É para mostrar a galeria de selos
			else
			{
				// Seta que ja mostrou a galeria de selos
				PlayerPrefs.SetInt("iIsToShowStampGallery", 0);

				// Carrega a tela de selos
				enum_currentScreen = Screens.Stamps;
			}
		}

		// Muda a tela
		ChangeScreen ();
	}

	/// <summary>
	/// Loads the language.
	/// </summary>
	private void LoadLanguage ()
	{
		// Carrega a tela de home
		enum_currentScreen = Screens.Home;

		// Verifica se foi escolhido o idioma Portuguese.
		if (PlayerPrefs.GetInt("LanguageSelected") == 1)
		{
			// Seta que foi selecionado o idioma Portuguese.
			i_languageSelected = 1;

			// Seta o idioma para português
			Language.SwitchLanguage(LanguageCode.PT);
		}
		
		// Verifica se foi escolhido o idioma English.
		else if (PlayerPrefs.GetInt("LanguageSelected") == 2) 
		{
			// Seta que foi selecionado o idioma English.
			i_languageSelected = 2;

			/// Seta o idioma para inglês
			Language.SwitchLanguage(LanguageCode.EN);
		}

		// Nenhum idioma foi selecionado
		else
		{
			// Carrega a tela de selecionar idioma
			enum_currentScreen = Screens.LanguageSelection;
		}
	}

	public int GetLanguage ()
	{
		return i_languageSelected;	
	}

	#region case void SetScreen
	/// <summary>
	/// Sets the screen.
	/// </summary>
	public void SetAction (string strActionName)
	{
		// Verifica se os botoes da tela estah ativos.
		if (b_isScreenButtonsActive)
		{
			// Verifica qual eh o Nome da Acao.
			switch (strActionName)
			{
				// Caso seja a tela de HOME.
			case "Home":
				
				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Seta que a camera atual eh de HOME.
				enum_currentScreen = Screens.Home;

				break;

				// Caso seja a tela de SELEÇÃO DE EPISÓDIOS.
			case "EpisodesSelection":
				
				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Seta que a camera atual eh de SELEÇÃO DE EPISÓDIOS.
				enum_currentScreen = Screens.EpisodesSelection;

				// ATIVA barra.
				go_interfaceBarBackground.SetActive(true);

				// Altera para qual tela o botao de back deve ir.
				SetInterfaceBar("backToHome");

				break;

				// Caso seja a tela de Creditos.
			case "Creditos":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Seta que a camera atual eh de Creditos.
				enum_currentScreen = Screens.Creditos;

				break;

				// Caso seja a tela de OPÇÕES.
			case "Options":
				
				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Seta que a camera atual é de OPÇÕES.
				enum_currentScreen = Screens.Options;

				// Altera para qual tela o botao de back deve ir.
				SetInterfaceBar("backToHome");

				break;

				// Caso seja a tela dos SELOS.
			case "Stamps":
				
				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Seta que a camera atual é de SELOS.
				enum_currentScreen = Screens.Stamps;

				// Altera para qual tela o botao de back deve ir.
				SetInterfaceBar("backToHome");

				break;

			case "Back":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Verifica se não está na tela de créditos
				if (enum_currentScreen != Screens.Creditos)
				{
					// Seta que a camera atual é de home
					enum_currentScreen = Screens.Home;
					
					// Altera para qual tela o botao de back deve ir
					SetInterfaceBar("info");
				}

				// Está na tela de créditos
				else
				{
					// Seta que a camera atual é de options
					enum_currentScreen = Screens.Options;
				}

				break;

			default:
				
				break;
			}

			// Muda a tela
			ChangeScreen();
		}
	}
	#endregion

	private void SetInterfaceBar (string strActionName) {

		// Verifica qual eh o Nome da Acao.
		switch (strActionName)
		{
			// Caso info.
		case "info":

			// Desativa o botao back.
			go_backButton.SetActive(false);

			// Ativa o botao info.
			go_infoButton.SetActive(true);
			
			break;

			// Caso backToHome.
		case "backToHome":

			// Ativa o botao back.
			go_backButton.SetActive(true);

			// Desativa o botao info.
			go_infoButton.SetActive(false);

			break;

		default:
			
			break;
		}
	}

	/// <summary>
	/// Raises the application quit event.
	/// </summary>
	void OnApplicationQuit ()
	{
		// DELETA a Key da Splash.
		PlayerPrefs.DeleteKey ("iIsSplashShowed");
	}
}