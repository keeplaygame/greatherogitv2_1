﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class StampGalleryController : MonoBehaviour 
{
	[SerializeField]
	// Guarda a quantidade dos selos
	private Text[] array_text_StampQuantity = new Text[10];

	// Guarda o nome dos selos
	private string[] array_str_stampName = new string[10];

	[SerializeField]
	// Guarda os objetos dos numeros dos selos
	private GameObject[] array_go_stampNumber = new GameObject[10];

	[SerializeField]
	// Recebe o sfxController
	private SFXController sfxController;

	// Use this for initialization
	void Awake () 
	{
		// Seta os nomes dos selos
		array_str_stampName[0] = "Purity";
		array_str_stampName[1] = "Confidence";
		array_str_stampName[2] = "Humbleness";
		array_str_stampName[3] = "Devotion";
		array_str_stampName[4] = "Justice";
		array_str_stampName[5] = "Bravery";
		array_str_stampName[6] = "Obedience";
		array_str_stampName[7] = "Friendship";
		array_str_stampName[8] = "Kindness";
		array_str_stampName[9] = "Love";
	}
	
	/// <summary>
	/// Restarts the stamp gallery.
	/// </summary>
	public void RestartStampGallery ()
	{
		// Percorre todos os selos
		for (int i = 0; i < array_str_stampName.Length; i++)
		{
			// Seta o texto da quantidade de selos
			array_text_StampQuantity[i].text = PlayerPrefs.GetInt(array_str_stampName[i] + "Quantity", 0).ToString();

			// Verifica se ganhou um novo selo
			if (PlayerPrefs.GetInt(array_str_stampName[i] + "NewQuantity", 0) > 0)
			{
				// Toca a animação de ganhar um selo
				array_go_stampNumber[i].GetComponent<Animator>().Play("StampTextQuantityIncrease");

				// Verifica se a quantidade do selo é menor que 10
				if (PlayerPrefs.GetInt(array_str_stampName[i] + "Quantity", 0) < 10)
				{
					// Inicia a corotina para mudar a quantidade do selo
					StartCoroutine("ChangeQuantity", i);
				}

				// Já tem 10 selos
				else
				{
					// Seta que não tem mais novo selo
					PlayerPrefs.SetInt(array_str_stampName[i] + "NewQuantity", 0);
				}
			}
		}
	}

	/// <summary>
	/// Changes the quantity.
	/// </summary>
	/// <returns>The quantity.</returns>
	/// <param name="iStampNumber">I stamp number.</param>
	private IEnumerator ChangeQuantity (int iStampNumber)
	{
		// Aguarda meio segundo para incrementar a quantidade do selo
		yield return new WaitForSeconds(0.5f);

		// Toca o som de novo selo
		sfxController.SetPlayAudioNewStamp(1.0f);

		// Calcula a nova quantidade de selos
		int iNewQuantity = PlayerPrefs.GetInt (array_str_stampName [iStampNumber] + "NewQuantity") + PlayerPrefs.GetInt (array_str_stampName [iStampNumber] + "Quantity", 0);

		// Verifica se a quantidade nova de selos é maior que 10
		if (iNewQuantity > 10) 
		{
			// Seta que a nova quantidade são 10 selos
			iNewQuantity = 10;
		}

		// Guarda a nova quantidade do selo
		PlayerPrefs.SetInt (array_str_stampName [iStampNumber] + "Quantity", iNewQuantity);

		// Atualiza o texto com a quantidade certa
		array_text_StampQuantity[iStampNumber].text = PlayerPrefs.GetInt(array_str_stampName[iStampNumber] + "Quantity", 0).ToString();

		// Seta que não tem mais novo selo
		PlayerPrefs.SetInt(array_str_stampName[iStampNumber] + "NewQuantity", 0);
	}
}
